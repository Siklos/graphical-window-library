/**
 * @file	ei_widget_extended.h
 * @brief	Configure extensions of a widget
 */

#ifndef EI_WIDGET_EXTENDED_H
#define EI_WIDGET_EXTENDED_H

#include <stdlib.h>

#include "ei_widget.h"

/**
 * @brief	A struct with an extension of parameters
 */
typedef struct ei_widget_extended_t
{
	ei_widget_t widget; ///< A widget to extend
	ei_callback_t destroy_cb; ///<	A callback function called after the destruction of the widget
	void* destroy_cb_param; ///< The parameters of the destroy callback function
} ei_widget_extended_t;


/**
 * \brief			Configures post widget destroy callback function to be called after a widget is destroyed.
 *
 * @param	widget		The widget to configure.
 *
 * @param	callback	The callback function that will be called after the destruction of the widget.
 *
 * @param	user_param	The parameters used in the callback function as its arguments.
 *
 */
void ei_widget_set_destroy_cb	(ei_widget_t*		widget,
				 ei_callback_t		callback,
				 void*			user_param);
#endif /* ifndef EI_WIDGET_EXTENDED_H */
