/**
 * @file	bresenham_utils.h
 *
 * @brief	Utils for line computing using the bresenham algorithm.
 */
#ifndef BRESENHAM_UTILS
#define BRESENHAM_UTILS

#include "ei_types.h"
#include "hw_interface.h"

/**
 * \brief		Draws a line following the bresenham method. Use this function if the x derivation is higher.
 *
 * @param	surface	The surface to draw into.
 * @param	point0	The starting point of the line.
 * @param	point1	The ending point of the line.
 * @param	dy	The y derivation.
 * @param	dx	The x derivation.
 * @param	color	The color used to draw.
 * @param	clipper	The clipper used to crop the line.
 *
 */
void draw_line_bresenham_dx(ei_surface_t surface, ei_point_t point0,
			    ei_point_t point1, int dy, int dx,
			    const ei_color_t *color, const ei_rect_t *clipper);

/**
 * \brief		Draws a line following the bresenham method. Use this function if the x derivation is higher.
 *
 * @param	surface	The surface to draw into.
 * @param	point0	The starting point of the line.
 * @param	point1	The ending point of the line.
 * @param	dx	The x derivation.
 * @param	dy	The y derivation.
 * @param	color	The color used to draw.
 * @param	clipper	The clipper used to crop the line.
 *
 */
void draw_line_bresenham_dy(ei_surface_t surface, ei_point_t point0,
			    ei_point_t point1, int dx, int dy,
			    const ei_color_t *color, const ei_rect_t *clipper);

#endif /* ifndef BRESENHAM_UTILS */
