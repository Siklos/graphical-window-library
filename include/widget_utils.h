/**
 *  @file	widget_utils.h
 *  @brief	Utils for a widget.
 *
 */

#ifndef WIDGET_UTILS_H
#define WIDGET_UTILS_H

#include "ei_types.h"
#include "ei_widget.h"
#include "hw_interface.h"

/**
* \brief			Returns the lighten version of the color set in parameter.
*
* @param	color		The color to lighten.
*
* @return			The lighten version of the color.
*/
ei_color_t lighten_color(ei_color_t);

/**
* \brief			Returns the darken version of the color set in parameter.
*
* @param	color		The color to darken.
*
* @return			The darken version of the color.
*/
ei_color_t darken_color(ei_color_t);

/**
 * \brief			Applies and returns a color with the gamma applied
 *				to.
 *
 * @param	color		The color to apply the gamma.
 *
 * @param	gamma		The value of the gamma.
 *
 * @return			A new color with the gamma applied to.
 */
ei_color_t color_plus(ei_color_t color, float gamma);

/**
 * \brief			Attaches an anchor to a rectangle. Return a new
 *				value for the top_left of the rectangle to
 *				reposition.
 *
 * @param	anchor		A anchor to attach to the widget.
 *
 * @param	rect		A rect to attach the anchor.
 *
 * @return			A point corresponding to the new top_left of the
 *				rectangle.
 */
ei_point_t apply_anchor(ei_anchor_t anchor, ei_rect_t rect);

/**
 * \brief			Computes the anchor of the image, i.e. where it is placed within the widget
 *				when the size of the widget is bigger than the size of the image.
 *
 * @param	anchor		How to anchor the widget to the position of the parent.
 *
 * @param	parent		the rectangular form of the parent in the root surface.
 *
 * @param	img_text_size	Size of the text/image.
 *
 * @param	corner_radius	(Optional) Shift the point if a corner is
 *				different from 0.
 *
 * @return			A point aligned from the anchor of the parent.
 */
ei_point_t position_from_anchor(ei_anchor_t anchor, ei_rect_t parent,
				ei_size_t img_text_size, int corner_radius);

/**
 * \brief			Draws the children of the widget.
 *
 * @param	widget		The parent widget to draw its children.
 *
 * @param	surface		The surface to draw into.
 *
 * @param	pick_surface	The pick surface to draw the pick_colors into.
 *
 */
void draw_children(ei_widget_t *widget, ei_surface_t surface,
		   ei_surface_t pick_surface);

/**
 * \brief			Sets the widget to the front of the widgets within the parent.
 *
 * @param	widget		The widget to set to the front.
 *
 */
void set_in_front(ei_widget_t *widget);

/**
 * @brief			Inverts the relief sent in parameter.
 *
 * @param	actual	The relief to invert.
 *
 */
ei_relief_t inverse_relief(ei_relief_t actual);

ei_rect_t clip_clipper(ei_rect_t content, ei_rect_t *clipper);

#endif
