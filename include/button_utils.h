/**
 *  @file	button_utils.h
 *  @brief	Manages the main steps of drawing a button: initialization, drawing,
 *		resource freeing.
 *
 */

#ifndef BUTTON_UTILS_H
#define BUTTON_UTILS_H

#include "ei_types.h"
#include "ei_draw.h"
#include "hw_interface.h"

/**
 * \brief				Returns a linked list of point representing an arc of a
 *					rounded polygon.
 *
 * @param	centre			The center of the rounded polygon.
 *
 * @param	rayon			The value of the radius.
 *
 * @return	ei_linked_point_t	A linked list of points representing an
 *					arc of a rounded polygon.
 */
ei_linked_point_t *arc_quartile_1(ei_point_t centre, int rayon);

/**
 * \brief				Returns a linked list of point representing an arc of a
 *					rounded polygon.
 *
 * @param	centre			The center of the rounded polygon.
 *
 * @param	rayon			The value of the radius.
 *
 * @return	ei_linked_point_t	A linked list of points representing an
 *					arc of a rounded polygon.
 */
ei_linked_point_t *arc_quartile_2(ei_point_t centre, int rayon);

/**
 * \brief				Returns a linked list of point representing an arc of a
 *					rounded polygon.
 *
 * @param	centre			The center of the rounded polygon.
 *
 * @param	rayon			The value of the radius.
 *
 * @return	ei_linked_point_t	A linked list of points representing an
 *					arc of a rounded polygon.
 */
ei_linked_point_t *arc_quartile_3(ei_point_t centre, int rayon);

/**
 * \brief				Returns a linked list of point representing an arc of a
 *					rounded polygon.
 *
 * @param	centre			The center of the rounded polygon.
 *
 * @param	rayon			The value of the radius.
 *
 * @return	ei_linked_point_t	A linked list of points representing an
 *					arc of a rounded polygon.
 */
ei_linked_point_t *arc_quartile_4(ei_point_t centre, int rayon);

/**
 * \brief				Returns a linked list of points
 *					representing a rounded polygon.
 *
 * @param	button_size		The size of the button.
 *
 * @param	radius_angle		The radius of a corner.
 *
 * @return	ei_linked_point_t	A linked list of points representing a
 *					rounded polygon.
 */
ei_linked_point_t *rounded_frame(ei_rect_t button_size, int radius_angle);

/**
 * \brief				Returns a linked list of points
 *					representing the upper part of a button.
 *
 * @param	button_size		The size of the button.
 *
 * @param	radius_angle		The radius of a corner.
 *
 * @return	ei_linked_point_t	A linked list of points representing the
 *					upper part of a button.
 */
ei_linked_point_t *button_up(ei_rect_t button_size, int radius_angle);

/**
 * \brief				Returns a linked list of points
 *					representing the lower part of a button.
 *
 * @param	button_size		The size of the button.
 *
 * @param	radius_angle		The radius of a corner.
 *
 * @return	ei_linked_point_t	A linked list of points representing the
 *					lower part of a button.
 */
ei_linked_point_t *button_down(ei_rect_t button_size, int radius_angle);

/**
 * \brief				Returns a linked list of point representing an arc (1/8 of a circle) of a
 *					rounded polygon. (Top-right 1st/8)
 *
 * @param	centre			The center of the rounded polygon.
 *
 * @param	rayon			The value of the radius.
 *
 * @return	ei_linked_point_t	A linked list of points representing an
 *					arc of a rounded polygon.
 */
ei_linked_point_t *octant_one(ei_point_t centre, int rayon);

/**
 * \brief				Returns a linked list of point representing an arc (1/8 of a circle) of a
 *					rounded polygon. (Top-right 2nd/8)
 *
 * @param	centre			The center of the rounded polygon.
 *
 * @param	rayon			The value of the radius.
 *
 * @return	ei_linked_point_t	A linked list of points representing an
 *					arc of a rounded polygon.
 */
ei_linked_point_t *octant_two(ei_point_t centre, int rayon);

/**
 * \brief				Returns a linked list of point representing an arc (1/8 of a circle) of a
 *					rounded polygon. (Bottom-left 5th/8)
 *
 * @param	centre			The center of the rounded polygon.
 *
 * @param	rayon			The value of the radius.
 *
 * @return	ei_linked_point_t	A linked list of points representing an
 *					arc of a rounded polygon.
 */
ei_linked_point_t *octant_five(ei_point_t centre, int rayon);

/**
 * \brief				Returns a linked list of point representing an arc (1/8 of a circle) of a
 *					rounded polygon. (Bottom-left 6th/8)
 *
 * @param	centre			The center of the rounded polygon.
 *
 * @param	rayon			The value of the radius.
 *
 * @return	ei_linked_point_t	A linked list of points representing an
 *					arc of a rounded polygon.
 */
ei_linked_point_t *octant_six(ei_point_t centre, int rayon);

/**
* \brief				Draws a button on a given surface.
 *
 * @param	surface			The surface to write into.
 *
 * @param	color_button		The main color of the button.
 *
 * @param	color_down		The bottom color of the
 *					background of the button.
 *
 * @param	color_up		The upper color of the
 *					background of the button.
 *
 * @param	in_frame		A linked list of points to set in the inside part of the button.
 *
 * @param	down_frame		A linked list of points to set in the inside part of the button.
 *
 * @param	up_frame		A linked list of points to set in the upper part of the button.
 *
 * @param	button_size		The size of the button.
 *
 * @param	radius_angle		The radius of a corner of the button.
 *
 * @param	border_width		The width of the border of a button.
 *
 * @param	clipper			The rectangle to crop the button.
 *
 */
void draw_button(ei_surface_t surface, const ei_color_t color_button,
		 const ei_color_t color_down, const ei_color_t color_up,
		 ei_linked_point_t *in_frame, ei_linked_point_t *down_frame,
		 ei_linked_point_t *up_frame, ei_rect_t button_size,
		 int radius_angle, int border_width, const ei_rect_t *clipper);

/**
 * \brief			Draws a picking version of button into a pick_surface.
 *
 * @param	pick_surface	The pick_surface to draw into.
 *
 * @param	color_id	The pick color of the button.
 *
 * @param	frame		The polygon of the button.
 *
 * @param	button_size	The size of the button.
 *
 * @param	radius_angle	The radius of a corner.
 *
 * @param	clipper		The rectangle to crop the button.
 */
void pick_button(ei_surface_t pick_surface, const ei_color_t color_id,
		 ei_linked_point_t *frame, ei_rect_t button_size,
		 int radius_angle, const ei_rect_t *clipper);

/**
 * \brief				Returns a polygon representing the top bar for a
 *					toplevel.
 *
 * @param	toplevel_rect			The rectangle representing the whole area of the
 *					toplevel.
 *
 * @param	radius_angle		The radius of a corner.
 *
 * @return	ei_linked_point_t	The linked list of points representing
 *					the top bar.
 */
ei_linked_point_t *toplevel(ei_rect_t toplevel_rect, int radius_angle);

/**
 *  \brief			Draws a toplevel widget.
 *
 *  @param	surface		The surface to draw into.
 *
 *  @param	color		The color of the toplevel.
 *
 *  @param	toplevel_rect	The whole area of the toplevel.
 *
 *  @param	radius_angle	The radius of a corner.
 *
 *  @param	clipper		The rectangle to crop the button.
 */
void draw_toplevel(ei_surface_t surface, const ei_color_t color,
		   ei_rect_t toplevel_rect, int radius_angle,
		   const ei_rect_t *clipper);

/**
 *  \brief			Frees a linked list of points.
 *
 * @param	linked_point	The linked list of points to free.
 *
 */
void free_linked_point(ei_linked_point_t *linked_point);

#endif
