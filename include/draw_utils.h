/**
 *  @file	draw_utils.h
 *
 *  @brief	Utils for drawing a pixel into a surface.
 *
 */

#ifndef DRAW_UTILS_H
#define DRAW_UTILS_H

#define SAME_COLOR(a, b)                                                       \
	(((a.red == b.red) && (a.blue == b.blue) && (a.green == b.green) &&    \
	  (a.alpha == b.alpha)) ?                                              \
		 (1) :                                                         \
		 (0))

#include "ei_types.h"
#include "hw_interface.h"

/**
 * \brief			Returns the coresponding color from a pixel.
 *
 * @param	surface		The surface to get its color indexes.
 *
 * @param	color		The buffer representing a color.
 *
 * @return	ei_color_t	Return the struct ei_color_t.
 */
ei_color_t buffer_to_color(ei_surface_t surface, uint32_t color);

/**
 * \brief			Returns the coresponding color from the coordinates of a pixel.
 *
 * @param	surface		The surface to get its color indexes.
 *
 * @param	x		The x position of the pixel.
 *
 * @param	y		The y position of the pixel.
 *
 * @return	ei_color_t	Return the struct ei_color_t.
 */
ei_color_t get_color(ei_surface_t surface, int x, int y);

/**
 * \brief			Returns a color buffer corresponding to color indexes pixel.
 *
 * @param	surface		The surface to get its color indexes.
 *
 * @param	color		A color to get its buffer value.
 *
 * @return	uint32_t		Return the color buffer.
 */
uint32_t ei_map_rgba(ei_surface_t surface, const ei_color_t *color);

/**
 * \brief			Returns the coresponding buffer color from a color according to the color indexes of the x, y of a given surface.
 *
 * @param	surface		The surface to get its color indexes.
 *
 * @param	x		The x position of a pixel.
 *
 * @param	y		The y position of a pixel.
 *
 * @param	color		A color to get its buffer value.
 *
 * @return	uint32_t		Return the color buffer.
 */
uint32_t get_map_rgba(ei_surface_t surface, int x, int y,
		      const ei_color_t *color);

/**
 * \brief			Checks if the (x, y) is in a given clipper.
 *
 * @param	x		The x position of a pixel.
 *
 * @param	y		The y position of a pixel.
 *
 * @param	clipper		The clipper of reference.
 *
 * @return	ei_bool_t	Return EI_TRUE if (x, y) is in the clipper, return EI_FALSE otherwise.
 */
ei_bool_t is_in_clipper(int x, int y, const ei_rect_t *clipper);

/**
 * \brief			Draws the pixel at the coordinates (x,y) if allowed.
 *
 * @param	surface		Where to draw the pixel.
 *
 * @param	x		Coordinate of the pixel on the x_axis.
 *
 * @param	y		Coordinate of the pixel on the y_axis.
 *
 * @param	color		The pixel's color.
 *
 * @param	clipper		Can be NULL, in this case, draw on the entire window.
 *				If not NULL, the drawing is restricted within this rectangle.
 */
void draw_pixel(ei_surface_t surface, int x, int y, const ei_color_t *color,
		const ei_rect_t *clipper);
#endif
