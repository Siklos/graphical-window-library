/**
 *  @file	polygon_utils.h
 *  @brief	List of utils used to draw a polygon
 *
 */

#ifndef POLYGON_UTILS_H
#define POLYGON_UTILS_H

#include "hw_interface.h"

/**
 * \brief	A linked list used to draw line
 */
typedef struct linked_list_tc {
	int y_max; ///< maximum height
	int32_t x_y_min; ///< x of the y_max
	int dx; ///< derivation in x
	int dy; ///< derivation in y
	int e; ///< error in derivation
	struct linked_list_tc *next; ///< next element in the list
} linked_list_tc_t;

/**
 *  \brief      inializes tc and get is height
 *
 *  @param      first_point     a linked list of points
 *  @param      ymin            minimum height of polygon
 *  @param      ymax            maximum height of polygon
 *  @return                     return an allocated tc list
 */
linked_list_tc_t **initialize_tc(const ei_linked_point_t *first_point,
				 int *ymin, int *ymax);

/**
 *  \brief      fill_tc with value point from a list
 *
 *  @param      first_point     a linked list of points
 *  @param      ymin            minimum heihgt of the polygon
 */
void fill_tc(linked_list_tc_t **tc, const ei_linked_point_t *first_point,
	     int ymin);

/**
 *  \brief      frees a linked_list_tc
 *
 *  @param      tca            the list to free
 */
void tca_free(linked_list_tc_t **tca);

/**
 *  \brief      removes element from tca if their eqal to y
 *
 *  @param      tca             the list
 *  @param      y               current heihgt
 */
void tca_remove_equal(linked_list_tc_t **tca, int y);

/**
 *  \brief      fills a scanline of the screen to draw a polygon
 *
 *  @param      surface         the surface to fill
 *  @param      color           fill the scanline with this color
 *  @param      tca             current line
 *  @param      y               height of line to fill
 *  @param      clipper         clipper who bound the zone we can fill
 */
void fill_polygon_scanline(ei_surface_t surface, ei_color_t color,
			   linked_list_tc_t *tca, int y,
			   const ei_rect_t *clipper);

/**
 *  \brief      splits a tc into two tc to merge sort them
 *
 *  @param      src         original list
 *  @param      front       first half of the list
 *  @param      back        last half of the list
 */
void tc_split(linked_list_tc_t *src, linked_list_tc_t **front,
	      linked_list_tc_t **back);


/**
 *  \brief      merges the list and sort it
 *
 *  @param      list1        first half of the list
 *  @param      list2        second half of the list
 */
linked_list_tc_t *tc_merge_and_sort(linked_list_tc_t *list1,
				    linked_list_tc_t *list2);


/**
 *  \brief      merges sort a list
 *
 *  @param      head        head of the list to sort
 */
void tc_merge_sort(linked_list_tc_t **head);

#endif /* ifndef POLYGON_UTILS_H */
