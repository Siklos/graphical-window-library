/**
 * @file	macro.h
 * @brief	A regroupment of macros.
 */

#ifndef MACRO_H
#define MACRO_H
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#endif
