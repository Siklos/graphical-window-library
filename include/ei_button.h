/**
 *  @file	ei_button.h
 *  @brief	Definition and registration of button class.
 *
 */

#ifndef EI_BUTTON_H
#define EI_BUTTON_H

#include <stdlib.h>

#include "hw_interface.h"
#include "ei_draw.h"
#include "ei_widgetclass.h"
#include "ei_widget.h"
#include "ei_types.h"
#include "ei_event.h"
#include "ei_widget_extended.h"

extern ei_widgetclass_t button_class;

/**
 * \brief	Fields to all types of a button widget
 */
typedef struct {
	ei_widget_extended_t widget; 	///< The base fields of an extended widget.
	ei_color_t bg_color; ///< The background color of the widget.
	int border_width; ///< The border width of the widget.
	ei_relief_t relief; ///< The relief of the border.

	char *text; ///< The text to be displayed. If NULL, no text will be displayed.
	ei_font_t text_font; ///< The font of the text.
	ei_color_t text_color; ///< The text color.
	ei_anchor_t text_anchor; ///< The anchor of the text within the widget.

	ei_surface_t *img; ///< The surface representing the picture.
	ei_rect_t *img_rect; ///< The rectangle to crop the surface. If NULL, the whole surface will be taken.
	ei_anchor_t img_anchor; ///< The anchor of the picture withing the widget.

	int corner_radius; ///< The radius of a corner of a button.
	ei_callback_t callback; ///< The function to be executed when the button is pressed.
	void *user_param; ///< The parameters of the callback function.

} ei_button_widget_t;

/**
 * \brief	A function that allocates a block of memory that is big enough to store the
 *		attributes of a widget of a class. After allocation, the function *must*
 *		initialize the memory to 0.
 *
 * @return		A block of memory with all bytes set to 0.
 */
void *ei_button_allocfunc();

/**
 * \brief	A function that releases the memory used by a widget before it is destroyed.
 *		The \ref ei_widget_t structure itself, passed as parameter, must *not* by freed by
 *		these functions. Can be set to NULL in \ref ei_widgetclass_t if no memory is used by
 *		a class of widget.
 *
 * @param	widget		The widget which resources are to be freed.
 */
void ei_button_releasefunc(struct ei_widget_t *widget);

/**
 * \brief	A function that draws widgets of a class.
 *
 * @param	widget		A pointer to the widget instance to draw.
 * @param	surface		Where to draw the widget. The actual location of the widget in the
 *				surface is stored in its "screen_location" field.
 * @param	pick_surface	Where to draw the pick version of the widget.
 * @param	clipper		If not NULL, the drawing is restricted within this rectangle
 *				(expressed in the surface reference frame).
 */
void ei_button_drawfunc(ei_widget_t *widget, ei_surface_t surface,
			ei_surface_t pick_surface, ei_rect_t *clipper);

/**
 * \brief	A function that sets the default values for a class.
 *
 * @param	widget		A pointer to the widget instance to intialize.
 */
void ei_button_setdefaultsfunc(ei_widget_t *widget);

/**
 * \brief 	A function that is called to notify the widget that its geometry has been modified
 *		by its geometry manager. Can set to NULL in \ref ei_widgetclass_t.
 *
 * @param	widget		The widget instance to notify of a geometry change.
 * @param	rect		The new rectangular screen location of the widget
 *				(i.e. = widget->screen_location).
 */
void ei_button_geomnotifyfunc(ei_widget_t *widget, ei_rect_t rect);

/**
 * @brief	A function that is called in response to an event. This function 
 *		is internal to the library. It implements the generic behavior of
 *		a widget (for example a button looks sunken when clicked)
 *
 * @param	widget		The widget for which the event was generated.
 * @param	event		The event containing all its parameters (type, etc.)
 *
 * @return			A boolean telling if the event was consumed by the callback or not.
 *				If TRUE, the library does not try to call other callbacks for this
 *				event. If FALSE, the library will call the next callback registered
 *				for this event, if any.
 *				Note: The callback may execute many operations and still return
 *				FALSE, or return TRUE without having done anything.
 */
ei_bool_t ei_button_handlefunc(ei_widget_t *widget, ei_event_t *event);

#endif
