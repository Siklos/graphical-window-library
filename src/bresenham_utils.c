/**
 * @file	bresenham_utils.c
 *
 * @brief	Utils for line computing using the bresenham algorithm.
 */

#include "ei_types.h"
#include "hw_interface.h"

#include "draw_utils.h"

/**
 * \brief		Draws a line following the bresenham method. Use this function if the x derivation is higher.
 *
 * @param	surface	The surface to draw into.
 * @param	point0	The starting point of the line.
 * @param	point1	The ending point of the line.
 * @param	dy	The y derivation.
 * @param	dx	The x derivation.
 * @param	color	The color used to draw.
 * @param	clipper	The clipper used to crop the line.
 *
 */
void draw_line_bresenham_dx(ei_surface_t surface, ei_point_t point0,
			    ei_point_t point1, int dy, int dx,
			    const ei_color_t *color, const ei_rect_t *clipper)
{
	int increment = 1;
	if (dy < 0) {
		increment = -1;
		dy = -dy;
	}
	int e = 0;
	for (; point0.x < point1.x; point0.x++) {
		draw_pixel(surface, point0.x, point0.y, color, clipper);
		e += dy;
		if (2 * e >= dx) {
			point0.y += increment;
			e -= dx;
		}
	}
}

/**
 * \brief		Draws a line following the bresenham method. Use this function if the x derivation is higher.
 *
 * @param	surface	The surface to draw into.
 * @param	point0	The starting point of the line.
 * @param	point1	The ending point of the line.
 * @param	dx	The x derivation.
 * @param	dy	The y derivation.
 * @param	color	The color used to draw.
 * @param	clipper	The clipper used to crop the line.
 *
 */
void draw_line_bresenham_dy(ei_surface_t surface, ei_point_t point0,
			    ei_point_t point1, int dx, int dy,
			    const ei_color_t *color, const ei_rect_t *clipper)
{
	int increment = 1;
	if (dx < 0) {
		increment = -1;
		dx = -dx;
	}
	int e = 0;
	for (; point0.y < point1.y; point0.y++) {
		draw_pixel(surface, point0.x, point0.y, color, clipper);
		e += dx;
		if (2 * e >= dy) {
			point0.x += increment;
			e -= dy;
		}
	}
}
