/**
 *  @file	ei_frame.c
 *  @brief	Definition and registration of frame class.
 *
 */

#include <stdlib.h>
#include <stdio.h>

#include "hw_interface.h"
#include "ei_draw.h"
#include "ei_widget.h"
#include "ei_types.h"
#include "ei_frame.h"
#include "ei_application.h"
#include "ei_utils.h"
#include "widget_utils.h"
#include "draw_utils.h"

#include "macro.h"

/**
 * \brief			Draws the border of the widget from a widget relief parameter.
 *
 * @param	widget		The widget to draw its borders.
 *
 * @param	surface		The surface to draw into.
 *
 * @param	ref_point	A reference point corresponding to the top_left of the border.
 *
 * @param	clipper		A clipper to crop the relief.
 *
 */
void draw_frame_relief(ei_widget_t *widget, ei_surface_t *surface,
		       ei_point_t ref_point, ei_rect_t *clipper);

/**
 * \brief	A structure that stores information about the frame class.
 */
ei_widgetclass_t frame_class = { "frame",
				 ei_frame_allocfunc,
				 ei_frame_releasefunc,
				 ei_frame_drawfunc,
				 ei_frame_setdefaultsfunc,
				 ei_frame_geomnotifyfunc,
				 NULL,
				 NULL };

/**
 * \brief	A function that allocates a block of memory that is big enough to store the
 *		attributes of a widget of a class. After allocation, the function *must*
 *		initialize the memory to 0.
 *
 * @return		A block of memory with all bytes set to 0.
 */
void *ei_frame_allocfunc()
{
	ei_frame_widget_t *pointer_ei_frame_widget =
		calloc(1, sizeof(ei_frame_widget_t));
	return pointer_ei_frame_widget;
}

/**
 * \brief	A function that releases the memory used by a widget before it is destroyed.
 *		The \ref ei_widget_t structure itself, passed as parameter, must *not* be freed by
 *		these functions. Can be set to NULL in \ref ei_widgetclass_t if no memory is used by
 *		a class of widget.
 *
 * @param	widget		The widget which resources are to be freed.
 */
void ei_frame_releasefunc(struct ei_widget_t *widget)
{
	if (widget == NULL)
		return;
	ei_widget_extended_t *extended = (ei_widget_extended_t *) widget;
	if (extended->destroy_cb != NULL)
		extended->destroy_cb(widget, NULL, extended->destroy_cb_param);
	ei_frame_widget_t *frame = (ei_frame_widget_t *) widget;
	if (frame->img_rect != NULL)
		free(frame->img_rect);
	if (frame->text != NULL)
		free(frame->text);
	widget->wclass = NULL;
}

/**
 * \brief	A function that draws widgets of a class.
 *
 * @param	widget		A pointer to the widget instance to draw.
 * @param	surface		Where to draw the widget. The actual location of the widget in the
 *				surface is stored in its "screen_location" field.
 * @param	pick_surface	Where to draw the pick version of the widget.
 * @param	clipper		If not NULL, the drawing is restricted within this rectangle
 *				(expressed in the surface reference frame).
 */
void ei_frame_drawfunc(ei_widget_t *widget, ei_surface_t surface,
		       ei_surface_t pick_surface, ei_rect_t *clipper)
{
	ei_frame_widget_t *frame = (ei_frame_widget_t *)widget;

	ei_rect_t frame_clipper = widget->screen_location;
	if (widget->screen_location.size.height == 0 &&
	    widget->screen_location.size.width == 0)
		return;

	ei_size_t size = widget->screen_location.size;

	if (!is_in_clipper(widget->screen_location.top_left.x,
			   widget->screen_location.top_left.y, clipper)) {
		frame_clipper.top_left.x = clipper->top_left.x;
		frame_clipper.top_left.y = clipper->top_left.y;
	}

	if (clipper->size.width <= widget->screen_location.size.width)
		frame_clipper.size.width = clipper->size.width -
					   frame_clipper.top_left.x +
					   clipper->top_left.x;
	if (clipper->size.height <= widget->screen_location.size.height)
		frame_clipper.size.height = clipper->size.height -
					    frame_clipper.top_left.y +
					    clipper->top_left.y;

	hw_surface_lock(surface);

	ei_rect_t inside_border = frame_clipper;

	inside_border.top_left.x += frame->border_width;
	inside_border.top_left.y += frame->border_width;
	inside_border.size.height -= frame->border_width << 1;
	inside_border.size.width -= frame->border_width << 1;

	ei_fill(surface, &(frame->bg_color), &frame_clipper);

	hw_surface_lock(pick_surface);
	ei_fill(pick_surface, widget->pick_color, &frame_clipper);
	hw_surface_unlock(pick_surface);

	if (frame->img != NULL) {
		ei_size_t img_size = hw_surface_get_size(frame->img);
		ei_rect_t img_position = { inside_border.top_left, inside_border.size };
		if (img_size.width < inside_border.size.width ||
		    img_size.height < size.height) {
			img_position.top_left = position_from_anchor(
				frame->text_anchor, inside_border, img_size, 0);
		}
		ei_copy_surface(surface, &img_position, frame->img,
				frame->img_rect, EI_TRUE);
	}
	if (frame->text) {
		ei_point_t where = inside_border.top_left;
		ei_size_t text_size;
		hw_text_compute_size(frame->text, frame->text_font,
				     &text_size.width, &text_size.height);
		if (text_size.width < inside_border.size.width ||
		    text_size.height < size.height) {
			where = position_from_anchor(frame->text_anchor,
						     inside_border, text_size,
						     0);
		}

		ei_draw_text(surface, &where, frame->text, frame->text_font,
			     &(frame->text_color), &inside_border);
	}

	if (frame->relief != ei_relief_none) {
		draw_frame_relief(widget, &surface, frame_clipper.top_left,
				  &frame_clipper);
	}

	hw_surface_unlock(surface);
	draw_children(widget, surface, pick_surface);
}

/**
 * \brief			Draw the border of the widget from a widget relief parameter.
 *
 * @param	widget		The widget to draw its borders.
 *
 * @param	surface		The surface to draw into.
 *
 * @param	ref_point	A reference point corresponding to the top_left of the border.
 *
 * @param	clipper		A clipper to crop the relief.
 *
 */
void draw_frame_relief(ei_widget_t *widget, ei_surface_t *surface,
		       ei_point_t ref_point, ei_rect_t *clipper)
{
	ei_frame_widget_t *frame = (ei_frame_widget_t *)widget;
	//left
	ei_color_t light = color_plus(frame->bg_color, 0.70);
	ei_color_t dark = color_plus(frame->bg_color, 1.80);
	ei_color_t top_color;
	ei_color_t bottom_color;
	if (frame->relief == ei_relief_raised) {
		top_color = light;
		bottom_color = dark;
	} else {
		top_color = dark;
		bottom_color = light;
	}

	ei_linked_point_t polys[4][5];

	ei_size_t size = widget->screen_location.size;

	polys[0][0].point = ref_point;
	polys[0][1].point = ei_point(ref_point.x, ref_point.y + size.height);
	polys[0][2].point =
		ei_point(ref_point.x + frame->border_width,
			 ref_point.y + size.height - frame->border_width);
	polys[0][3].point =
		ei_point(ref_point.x + frame->border_width, ref_point.y);
	polys[0][4].point = ref_point;

	polys[1][0].point =
		ei_point(ref_point.x + frame->border_width, ref_point.y);
	polys[1][1].point = ei_point(ref_point.x + size.width, ref_point.y);
	polys[1][2].point =
		ei_point(ref_point.x + size.width - frame->border_width,
			 ref_point.y + frame->border_width);
	polys[1][3].point = ei_point(ref_point.x + frame->border_width,
				     ref_point.y + frame->border_width);
	polys[1][4].point =
		ei_point(ref_point.x + frame->border_width, ref_point.y);

	polys[2][0].point = ei_point(ref_point.x + size.width, ref_point.y);
	polys[2][1].point =
		ei_point(ref_point.x + size.width, ref_point.y + size.height);
	polys[2][2].point =
		ei_point(ref_point.x + size.width - frame->border_width,
			 ref_point.y + size.height);
	polys[2][3].point =
		ei_point(ref_point.x + size.width - frame->border_width,
			 ref_point.y + frame->border_width);
	polys[2][4].point = ei_point(ref_point.x + size.width, ref_point.y);

	polys[3][0].point = ei_point(ref_point.x, ref_point.y + size.height);
	polys[3][1].point =
		ei_point(ref_point.x + size.width, ref_point.y + size.height);
	polys[3][2].point =
		ei_point(ref_point.x + size.width,
			 ref_point.y + size.height - frame->border_width);
	polys[3][3].point =
		ei_point(ref_point.x + frame->border_width,
			 ref_point.y + size.height - frame->border_width);
	polys[3][4].point = ei_point(ref_point.x, ref_point.y + size.height);

	for (int j = 0; j < 4; ++j) {
		ei_color_t *color = (j < 2) ? &(top_color) : &(bottom_color);
		int i = 1;
		for (; i <= 4; ++i) {
			polys[j][i - 1].next = &(polys[j][i]);
		}
		polys[j][i - 1].next = NULL;
		ei_draw_polygon(*surface, polys[j], *color, clipper);
	}
}

/**
 * \brief	A function that sets the default values for a class.
 *
 * @param	widget		A pointer to the widget instance to intialize.
 */
void ei_frame_setdefaultsfunc(ei_widget_t *widget)
{
	ei_frame_widget_t *frame = (ei_frame_widget_t *)widget;
	frame->bg_color = ei_default_background_color;
	frame->relief = ei_relief_none;
	frame->border_width = 0;
	frame->text = NULL;
	frame->text_font = ei_default_font;
	frame->text_color = ei_font_default_color;
	frame->text_anchor = ei_anc_center;
	frame->img = NULL;
	frame->img_rect = NULL;
	frame->img_anchor = ei_anc_center;
	widget->requested_size.height = 0;
	widget->requested_size.width = 0;
}

/**
 * \brief 	A function that is called to notify the widget that its geometry has been modified
 *		by its geometry manager. Can set to NULL in \ref ei_widgetclass_t.
 *
 * @param	widget		The widget instance to notify of a geometry change.
 * @param	rect		The new rectangular screen location of the widget
 *				(i.e. = widget->screen_location).
 */
void ei_frame_geomnotifyfunc(ei_widget_t *widget, ei_rect_t rect)
{
	ei_rect_t new_rect =
		ei_rect(ei_point(MIN(widget->screen_location.top_left.x,
				     rect.top_left.x),
				 MIN(widget->screen_location.top_left.y,
				     rect.top_left.y)),
			ei_size(MAX(widget->screen_location.top_left.x +
					    widget->screen_location.size.width,
				    rect.top_left.x + rect.size.width),
				MAX(widget->screen_location.top_left.y +
					    widget->screen_location.size.height,
				    rect.top_left.y + rect.size.height)));
	ei_app_invalidate_rect(&new_rect);
	widget->screen_location = rect;
}
