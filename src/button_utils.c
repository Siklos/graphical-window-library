/**
 *  @file	button_utils.c
 *  @brief	Manages the main steps of drawing a button: initialization, drawing,
 *		resource freeing.
 *
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "ei_types.h"
#include "ei_draw.h"
#include "button_utils.h"
#include "ei_utils.h"
#include "hw_interface.h"
#include "widget_utils.h"

#define PI 3.14159265358979323846

/**
 *  \brief		Merges two linked list of points together by appending the list2 to list1.
 *
 *  @param	list1	The linked list of points where to append to.
 *
 *  @param	list2	The linked list of points that will be appending.
 *
 */
void fusion(ei_linked_point_t *list1, ei_linked_point_t *list2)
{
	if (list1 == NULL) {
		if (list2 == NULL) {
			return;
		} else {
			list1 = list2;
		}
	} else if (list2 == NULL) {
		return;
	} else {
		ei_linked_point_t *current = list1;
		while (current->next != NULL) {
			current = current->next;
		}
		current->next = list2;
	}
}

/**
 *  \brief		Reverts a linked_list of points
 *
 *  @param	list	The linked list of points to revert.
 *
 *
 */
ei_linked_point_t *reverse_list(ei_linked_point_t *list)
{
	ei_linked_point_t *new_list = NULL;
	while (list) {
		ei_linked_point_t *next = list->next;
		list->next = new_list;
		new_list = list;
		list = next;
	}
	return new_list;
}

/**
 * \brief				Returns a linked list of point representing an arc of a
 *					rounded polygon.
 *
 * @param	centre			The center of the rounded polygon.
 *
 * @param	rayon			The value of the radius.
 *
 * @return	ei_linked_point_t	A linked list of points representing an
 *					arc of a rounded polygon.
 */
ei_linked_point_t *arc_quartile_1(ei_point_t centre, int rayon)
{
	int x = 0;
	int y = rayon;
	ei_linked_point_t octant_1;
	ei_linked_point_t octant_2;
	octant_1.point = ei_point(0, 0);
	octant_2.point = ei_point(0, 0);
	octant_1.next = NULL;
	octant_2.next = NULL;
	ei_linked_point_t *current_1 = &octant_1;
	ei_linked_point_t *current_2 = &octant_2;

	int m = 5 - 4 * rayon;
	while (x < y) {
		ei_linked_point_t *arc_1 =
			malloc(sizeof(struct ei_linked_point_t));
		ei_linked_point_t *arc_2 =
			malloc(sizeof(struct ei_linked_point_t));
		arc_1->point = ei_point(centre.x + y, centre.y - x);
		arc_2->point = ei_point(centre.x + x, centre.y - y);
		arc_1->next = NULL;
		arc_2->next = NULL;
		current_1->next = arc_1;
		current_2->next = arc_2;
		current_1 = current_1->next;
		current_2 = current_2->next;

		if (m > 0) {
			y -= 1;
			m -= 8 * y;
		}
		x = x + 1;
		m += 8 * x + 4;
	}
	ei_linked_point_t *arc_1 = malloc(sizeof(struct ei_linked_point_t));
	arc_1->point = ei_point(centre.x + y, centre.y - x);
	arc_1->next = NULL;
	current_1->next = arc_1;
	ei_linked_point_t *r_octant_2 = reverse_list(octant_2.next);
	fusion(octant_1.next, r_octant_2);
	return octant_1.next;
}

/**
 * \brief				Returns a linked list of point representing an arc of a
 *					rounded polygon.
 *
 * @param	centre			The center of the rounded polygon.
 *
 * @param	rayon			The value of the radius.
 *
 * @return	ei_linked_point_t	A linked list of points representing an
 *					arc of a rounded polygon.
 */
ei_linked_point_t *arc_quartile_2(ei_point_t centre, int rayon)
{
	int x = 0;
	int y = rayon;
	ei_linked_point_t octant_3;
	ei_linked_point_t octant_4;
	octant_3.point = ei_point(0, 0);
	octant_4.point = ei_point(0, 0);
	octant_3.next = NULL;
	octant_4.next = NULL;
	ei_linked_point_t *current_3 = &octant_3;
	ei_linked_point_t *current_4 = &octant_4;

	int m = 5 - 4 * rayon;
	while (x < y) {
		ei_linked_point_t *arc_3 =
			malloc(sizeof(struct ei_linked_point_t));
		ei_linked_point_t *arc_4 =
			malloc(sizeof(struct ei_linked_point_t));
		arc_3->point = ei_point(centre.x - x, centre.y - y);
		arc_4->point = ei_point(centre.x - y, centre.y - x);
		arc_3->next = NULL;
		arc_4->next = NULL;
		current_3->next = arc_3;
		current_4->next = arc_4;
		current_3 = current_3->next;
		current_4 = current_4->next;

		if (m > 0) {
			y -= 1;
			m -= 8 * y;
		}
		x = x + 1;
		m += 8 * x + 4;
	}
	ei_linked_point_t *arc_3 = malloc(sizeof(struct ei_linked_point_t));
	arc_3->point = ei_point(centre.x - x, centre.y - y);
	arc_3->next = NULL;
	current_3->next = arc_3;
	ei_linked_point_t *r_octant_4 = reverse_list(octant_4.next);
	fusion(octant_3.next, r_octant_4);
	return octant_3.next;
}

/**
 * \brief				Returns a linked list of point representing an arc of a
 *					rounded polygon.
 *
 * @param	centre			The center of the rounded polygon.
 *
 * @param	rayon			The value of the radius.
 *
 * @return	ei_linked_point_t	A linked list of points representing an
 *					arc of a rounded polygon.
 */
ei_linked_point_t *arc_quartile_3(ei_point_t centre, int rayon)
{
	int x = 0;
	int y = rayon;
	ei_linked_point_t octant_5;
	ei_linked_point_t octant_6;
	octant_5.point = ei_point(0, 0);
	octant_6.point = ei_point(0, 0);
	octant_5.next = NULL;
	octant_6.next = NULL;
	ei_linked_point_t *current_5 = &octant_5;
	ei_linked_point_t *current_6 = &octant_6;

	int m = 5 - 4 * rayon;
	while (x < y) {
		ei_linked_point_t *arc_5 =
			malloc(sizeof(struct ei_linked_point_t));
		ei_linked_point_t *arc_6 =
			malloc(sizeof(struct ei_linked_point_t));
		arc_5->point = ei_point(centre.x - y, centre.y + x);
		arc_6->point = ei_point(centre.x - x, centre.y + y);
		arc_5->next = NULL;
		arc_6->next = NULL;
		current_5->next = arc_5;
		current_6->next = arc_6;
		current_5 = current_5->next;
		current_6 = current_6->next;

		if (m > 0) {
			y -= 1;
			m -= 8 * y;
		}
		x = x + 1;
		m += 8 * x + 4;
	}
	ei_linked_point_t *arc_5 = malloc(sizeof(struct ei_linked_point_t));
	arc_5->point = ei_point(centre.x - y, centre.y + x);
	arc_5->next = NULL;
	current_5->next = arc_5;
	ei_linked_point_t *r_octant_6 = reverse_list(octant_6.next);
	fusion(octant_5.next, r_octant_6);
	return octant_5.next;
}

/**
 * \brief				Returns a linked list of point representing an arc of a
 *					rounded polygon.
 *
 * @param	centre			The center of the rounded polygon.
 *
 * @param	rayon			The value of the radius.
 *
 * @return	ei_linked_point_t	A linked list of points representing an
 *					arc of a rounded polygon.
 */
ei_linked_point_t *arc_quartile_4(ei_point_t centre, int rayon)
{
	int x = 0;
	int y = rayon;
	ei_linked_point_t octant_7;
	ei_linked_point_t octant_8;
	octant_7.point = ei_point(0, 0);
	octant_8.point = ei_point(0, 0);
	octant_7.next = NULL;
	octant_8.next = NULL;
	ei_linked_point_t *current_7 = &octant_7;
	ei_linked_point_t *current_8 = &octant_8;

	int m = 5 - 4 * rayon;
	while (x < y) {
		ei_linked_point_t *arc_7 =
			malloc(sizeof(struct ei_linked_point_t));
		ei_linked_point_t *arc_8 =
			malloc(sizeof(struct ei_linked_point_t));
		arc_7->point = ei_point(centre.x + x, centre.y + y);
		arc_8->point = ei_point(centre.x + y, centre.y + x);
		arc_7->next = NULL;
		arc_8->next = NULL;
		current_7->next = arc_7;
		current_8->next = arc_8;
		current_7 = current_7->next;
		current_8 = current_8->next;

		if (m > 0) {
			y -= 1;
			m -= 8 * y;
		}
		x = x + 1;
		m += 8 * x + 4;
	}
	ei_linked_point_t *arc_7 = malloc(sizeof(struct ei_linked_point_t));
	arc_7->point = ei_point(centre.x + x, centre.y + y);
	arc_7->next = NULL;
	current_7->next = arc_7;
	ei_linked_point_t *r_octant_8 = reverse_list(octant_8.next);
	fusion(octant_7.next, r_octant_8);
	return octant_7.next;
}

/**
 * \brief				Returns a linked list of point representing an arc (1/8 of a circle) of a
 *					rounded polygon. (Top-right 1st/8)
 *
 * @param	centre			The center of the rounded polygon.
 *
 * @param	rayon			The value of the radius.
 *
 * @return	ei_linked_point_t	A linked list of points representing an
 *					arc of a rounded polygon.
 */
ei_linked_point_t *octant_one(ei_point_t centre, int rayon)
{
	int x = 0;
	int y = rayon;
	ei_linked_point_t octant_1;
	octant_1.point = ei_point(0, 0);
	octant_1.next = NULL;
	ei_linked_point_t *current_1 = &octant_1;

	int m = 5 - 4 * rayon;
	while (x < y) {
		ei_linked_point_t *arc_1 =
			malloc(sizeof(struct ei_linked_point_t));
		arc_1->point = ei_point(centre.x + y, centre.y - x);
		arc_1->next = NULL;
		current_1->next = arc_1;
		current_1 = current_1->next;

		if (m > 0) {
			y -= 1;
			m -= 8 * y;
		}
		x = x + 1;
		m += 8 * x + 4;
	}
	ei_linked_point_t *arc_1 = malloc(sizeof(struct ei_linked_point_t));
	arc_1->point = ei_point(centre.x + y, centre.y - x);
	arc_1->next = NULL;
	current_1->next = arc_1;
	return octant_1.next;
}

/**
 * \brief				Returns a linked list of point representing an arc (1/8 of a circle) of a
 *					rounded polygon. (Top-right 2nd/8)
 *
 * @param	centre			The center of the rounded polygon.
 *
 * @param	rayon			The value of the radius.
 *
 * @return	ei_linked_point_t	A linked list of points representing an
 *					arc of a rounded polygon.
 */
ei_linked_point_t *octant_two(ei_point_t centre, int rayon)
{
	int x = 0;
	int y = rayon;
	ei_linked_point_t octant_2;
	octant_2.point = ei_point(0, 0);
	octant_2.next = NULL;
	ei_linked_point_t *current_2 = &octant_2;

	int m = 5 - 4 * rayon;
	while (x < y) {
		ei_linked_point_t *arc_2 =
			malloc(sizeof(struct ei_linked_point_t));
		arc_2->point = ei_point(centre.x + x, centre.y - y);
		arc_2->next = NULL;
		current_2->next = arc_2;
		current_2 = current_2->next;

		if (m > 0) {
			y -= 1;
			m -= 8 * y;
		}
		x = x + 1;
		m += 8 * x + 4;
	}
	ei_linked_point_t *arc_2 = malloc(sizeof(struct ei_linked_point_t));
	arc_2->point = ei_point(centre.x + y, centre.y - x);
	arc_2->next = NULL;
	current_2->next = arc_2;
	ei_linked_point_t *r_octant_2 = reverse_list(octant_2.next);
	return r_octant_2;
}

/**
 * \brief				Returns a linked list of point representing an arc (1/8 of a circle) of a
 *					rounded polygon. (Bottom-left 5th/8)
 *
 * @param	centre			The center of the rounded polygon.
 *
 * @param	rayon			The value of the radius.
 *
 * @return	ei_linked_point_t	A linked list of points representing an
 *					arc of a rounded polygon.
 */
ei_linked_point_t *octant_five(ei_point_t centre, int rayon)
{
	int x = 0;
	int y = rayon;
	ei_linked_point_t octant_5;
	octant_5.point = ei_point(0, 0);
	octant_5.next = NULL;
	ei_linked_point_t *current_5 = &octant_5;

	int m = 5 - 4 * rayon;
	while (x < y) {
		ei_linked_point_t *arc_5 =
			malloc(sizeof(struct ei_linked_point_t));
		arc_5->point = ei_point(centre.x - y, centre.y + x);
		arc_5->next = NULL;
		current_5->next = arc_5;
		current_5 = current_5->next;

		if (m > 0) {
			y -= 1;
			m -= 8 * y;
		}
		x = x + 1;
		m += 8 * x + 4;
	}
	ei_linked_point_t *arc_5 = malloc(sizeof(struct ei_linked_point_t));
	arc_5->point = ei_point(centre.x - y, centre.y + x);
	arc_5->next = NULL;
	current_5->next = arc_5;
	return octant_5.next;
}

/**
 * \brief				Returns a linked list of point representing an arc (1/8 of a circle) of a
 *					rounded polygon. (Bottom-left 6th/8)
 *
 * @param	centre			The center of the rounded polygon.
 *
 * @param	rayon			The value of the radius.
 *
 * @return	ei_linked_point_t	A linked list of points representing an
 *					arc of a rounded polygon.
 */
ei_linked_point_t *octant_six(ei_point_t centre, int rayon)
{
	int x = 0;
	int y = rayon;
	ei_linked_point_t octant_6;
	octant_6.point = ei_point(0, 0);
	octant_6.next = NULL;
	ei_linked_point_t *current_6 = &octant_6;

	int m = 5 - 4 * rayon;
	while (x < y) {
		ei_linked_point_t *arc_6 =
			malloc(sizeof(struct ei_linked_point_t));
		arc_6->point = ei_point(centre.x - x, centre.y + y);
		arc_6->next = NULL;
		current_6->next = arc_6;
		current_6 = current_6->next;

		if (m > 0) {
			y -= 1;
			m -= 8 * y;
		}
		x = x + 1;
		m += 8 * x + 4;
	}
	ei_linked_point_t *arc_6 = malloc(sizeof(struct ei_linked_point_t));
	arc_6->point = ei_point(centre.x - y, centre.y + x);
	arc_6->next = NULL;
	current_6->next = arc_6;
	current_6 = current_6->next;
	ei_linked_point_t *r_octant_6 = reverse_list(octant_6.next);
	return r_octant_6;
}

/**
 * \brief				Returns a linked list of points
 *					representing the upper part of a button.
 *
 * @param	button_size		The size of the button.
 *
 * @param	radius_angle		The radius of a corner.
 *
 * @return	ei_linked_point_t	A linked list of points representing the
 *					upper part of a button.
 */
ei_linked_point_t *button_up(ei_rect_t button_size, int radius_angle)
{
	ei_point_t centre_1 = { button_size.top_left.x +
					button_size.size.width - radius_angle,
				button_size.top_left.y + radius_angle };
	ei_point_t centre_2 = { button_size.top_left.x + radius_angle,
				button_size.top_left.y + radius_angle };
	ei_point_t centre_3 = {
		button_size.top_left.x + radius_angle,
		button_size.top_left.y + button_size.size.height - radius_angle
	};

	int h = (button_size.size.height < button_size.size.width ?
			 button_size.size.height :
			 button_size.size.width) >>
		1;
	ei_point_t r_side = { button_size.top_left.x + button_size.size.width -
				      h,
			      button_size.top_left.y + h };
	ei_point_t l_side = { button_size.top_left.x + h,
			      button_size.top_left.y + button_size.size.height -
				      h };

	ei_linked_point_t *linked_2 = octant_two(centre_1, radius_angle);
	ei_linked_point_t *transition_ur = malloc(sizeof(ei_linked_point_t));
	transition_ur->point = linked_2->point;
	transition_ur->next = NULL;
	ei_linked_point_t *linked_5 = octant_five(centre_3, radius_angle);
	ei_linked_point_t *ul_corner = arc_quartile_2(centre_2, radius_angle);

	ei_linked_point_t *r_transition = malloc(sizeof(ei_linked_point_t));
	ei_linked_point_t *l_transition = malloc(sizeof(ei_linked_point_t));

	r_transition->point = r_side;
	r_transition->next = transition_ur;
	l_transition->point = l_side;
	l_transition->next = r_transition;

	fusion(linked_5, l_transition);
	fusion(linked_2, ul_corner);
	fusion(linked_2, linked_5);

	return linked_2;
}

/**
 * \brief				Returns a linked list of points
 *					representing the lower part of a button.
 *
 * @param	button_size		The size of the button.
 *
 * @param	radius_angle		The radius of a corner.
 *
 * @return	ei_linked_point_t	A linked list of points representing the
 *					lower part of a button.
 */
ei_linked_point_t *button_down(ei_rect_t button_size, int radius_angle)
{
	ei_point_t centre_1 = { button_size.top_left.x +
					button_size.size.width - radius_angle,
				button_size.top_left.y + radius_angle };
	ei_point_t centre_3 = {
		button_size.top_left.x + radius_angle,
		button_size.top_left.y + button_size.size.height - radius_angle
	};
	ei_point_t centre_4 = {
		button_size.top_left.x + button_size.size.width - radius_angle,
		button_size.top_left.y + button_size.size.height - radius_angle
	};

	int h = (button_size.size.height < button_size.size.width ?
			 button_size.size.height :
			 button_size.size.width) >>
		1;
	ei_point_t r_side = { button_size.top_left.x + button_size.size.width -
				      h,
			      button_size.top_left.y + h };
	ei_point_t l_side = { button_size.top_left.x + h,
			      button_size.top_left.y + button_size.size.height -
				      h };

	ei_linked_point_t *linked_1 = octant_one(centre_1, radius_angle);
	ei_linked_point_t *linked_6 = octant_six(centre_3, radius_angle);
	ei_linked_point_t *transition_dl = malloc(sizeof(ei_linked_point_t));
	transition_dl->point = linked_6->point;
	transition_dl->next = NULL;
	ei_linked_point_t *dr_corner = arc_quartile_4(centre_4, radius_angle);

	ei_linked_point_t *r_transition = malloc(sizeof(ei_linked_point_t));
	ei_linked_point_t *l_transition = malloc(sizeof(ei_linked_point_t));

	r_transition->point = r_side;
	r_transition->next = l_transition;
	l_transition->point = l_side;
	l_transition->next = transition_dl;

	fusion(linked_1, r_transition);
	fusion(linked_6, dr_corner);
	fusion(linked_6, linked_1);

	return linked_6;
}

/**
 * \brief				Draws a button on a given surface.
 *
 * @param	surface			The surface to write into.
 *
 * @param	color_button		The main color of the button.
 *
 * @param	color_down		The bottom color of the
 *					background of the button.
 *
 * @param	color_up		The upper color of the
 *					background of the button.
 *
 * @param	in_frame		A linked list of points to set in the inside part of the button.
 *
 * @param	down_frame		A linked list of points to set in the inside part of the button.
 *
 * @param	up_frame		A linked list of points to set in the upper part of the button.
 *
 * @param	button_size		The size of the button.
 *
 * @param	radius_angle		The radius of a corner of the button.
 *
 * @param	border_width		The width of the border of a button.
 *
 * @param	clipper			The rectangle to crop the button.
 *
 */
void draw_button(ei_surface_t surface, const ei_color_t color_button,
		 const ei_color_t color_down, const ei_color_t color_up,
		 ei_linked_point_t *in_frame, ei_linked_point_t *down_frame,
		 ei_linked_point_t *up_frame, ei_rect_t button_size,
		 int radius_angle, int border_width, const ei_rect_t *clipper)
{
	ei_point_t in_top_left =
		ei_point(button_size.top_left.x + border_width,
			 button_size.top_left.y + border_width);
	ei_size_t in_size = ei_size(button_size.size.width - 2 * border_width,
				    button_size.size.height - 2 * border_width);
	ei_rect_t in_button = { in_top_left, in_size };

	in_frame = rounded_frame(in_button, radius_angle);
	down_frame = button_down(button_size, radius_angle);
	up_frame = button_up(button_size, radius_angle);

	ei_draw_polygon(surface, down_frame, color_down, clipper);
	ei_draw_polygon(surface, up_frame, color_up, clipper);
	ei_draw_polygon(surface, in_frame, color_button, clipper);

	free_linked_point(down_frame);
	free_linked_point(up_frame);
	free_linked_point(in_frame);
}

/**
 * \brief			Draws a picking version of button into a pick_surface.
 *
 * @param	pick_surface	The pick_surface to draw into.
 *
 * @param	color_id	The pick color of the button.
 *
 * @param	frame		The polygon of the button.
 *
 * @param	button_size	The size of the button.
 *
 * @param	radius_angle	The radius of a corner.
 *
 * @param	clipper		The rectangle to crop the button.
 */
void pick_button(ei_surface_t pick_surface, const ei_color_t color_id,
		 ei_linked_point_t *frame, ei_rect_t button_size,
		 int radius_angle, const ei_rect_t *clipper)
{
	frame = rounded_frame(button_size, radius_angle);
	ei_draw_polygon(pick_surface, frame, color_id, clipper);
	free_linked_point(frame);
}

/**
 * \brief				Returns a linked list of points
 *					representing a rounded polygon.
 *
 * @param	button_size		The size of the button.
 *
 * @param	radius_angle		The radius of a corner.
 *
 * @return	ei_linked_point_t	A linked list of points representing a
 *					rounded polygon.
 */
ei_linked_point_t *rounded_frame(ei_rect_t button_size, int radius_angle)
{
	ei_point_t centre_1 = { button_size.top_left.x +
					button_size.size.width - radius_angle,
				button_size.top_left.y + radius_angle };
	ei_point_t centre_2 = { button_size.top_left.x + radius_angle,
				button_size.top_left.y + radius_angle };
	ei_point_t centre_3 = {
		button_size.top_left.x + radius_angle,
		button_size.top_left.y + button_size.size.height - radius_angle
	};
	ei_point_t centre_4 = {
		button_size.top_left.x + button_size.size.width - radius_angle,
		button_size.top_left.y + button_size.size.height - radius_angle
	};

	ei_linked_point_t *list_1 = arc_quartile_1(centre_1, radius_angle);
	ei_linked_point_t *list_2 = arc_quartile_2(centre_2, radius_angle);
	ei_linked_point_t *list_3 = arc_quartile_3(centre_3, radius_angle);
	ei_linked_point_t *list_4 = arc_quartile_4(centre_4, radius_angle);

	ei_linked_point_t *transition = malloc(sizeof(ei_linked_point_t));
	transition->point =
		ei_point(button_size.top_left.x + button_size.size.width,
			 button_size.top_left.y + radius_angle);
	transition->next = NULL;

	fusion(list_4, transition);

	fusion(list_1, list_2);
	fusion(list_3, list_4);
	fusion(list_1, list_3);

	return list_1;
}

/**
 * \brief				Returns a polygon representing the top bar for a
 *					toplevel.
 *
 * @param	toplevel_rect		The rectangle representing the whole area of the
 *					toplevel.
 *
 * @param	radius_angle		The radius of a corner.
 *
 * @return	ei_linked_point_t	The linked list of points representing
 *					the top bar.
 */
ei_linked_point_t *toplevel(ei_rect_t toplevel_rect, int radius_angle)
{
	ei_point_t centre_1 = { toplevel_rect.top_left.x +
					toplevel_rect.size.width - radius_angle,
				toplevel_rect.top_left.y + radius_angle };

	ei_point_t centre_2 = { toplevel_rect.top_left.x + radius_angle,
				toplevel_rect.top_left.y + radius_angle };

	ei_linked_point_t *list_1 = arc_quartile_1(centre_1, radius_angle);
	ei_linked_point_t *list_2 = arc_quartile_2(centre_2, radius_angle);

	ei_point_t bottom_left =
		ei_point(toplevel_rect.top_left.x,
			 toplevel_rect.top_left.y + toplevel_rect.size.height);
	ei_point_t bottom_right =
		ei_point(toplevel_rect.top_left.x + toplevel_rect.size.width,
			 toplevel_rect.top_left.y + toplevel_rect.size.height);

	ei_linked_point_t *dl_transition = malloc(sizeof(ei_linked_point_t));
	ei_linked_point_t *dr_transition = malloc(sizeof(ei_linked_point_t));
	ei_linked_point_t *ur_transition = malloc(sizeof(ei_linked_point_t));

	dl_transition->point = bottom_left;
	dl_transition->next = dr_transition;
	dr_transition->point = bottom_right;
	dr_transition->next = ur_transition;
	ur_transition->point =
		ei_point(toplevel_rect.top_left.x + toplevel_rect.size.width,
			 toplevel_rect.top_left.y + radius_angle);
	ur_transition->next = NULL;

	fusion(list_2, dl_transition);
	fusion(list_1, list_2);

	return list_1;
}

/**
 *  \brief			Draws a toplevel widget.
 *
 *  @param	surface		The surface to draw into.
 *
 *  @param	color		The color of the toplevel.
 *
 *  @param	toplevel_rect	The whole area of the toplevel.
 *
 *  @param	radius_angle	The radius of a corner.
 *
 *  @param	clipper		The rectangle to crop the button.
 */
void draw_toplevel(ei_surface_t surface, const ei_color_t color,
		   ei_rect_t toplevel_rect, int radius_angle,
		   const ei_rect_t *clipper)
{
	ei_linked_point_t *linked_toplevel =
		toplevel(toplevel_rect, radius_angle);
	ei_draw_polygon(surface, linked_toplevel, color, clipper);
	free_linked_point(linked_toplevel);
}

/**
 *  \brief			Frees a linked list of points.
 *
 * @param	linked_point	The linked list of points to free.
 *
 */
void free_linked_point(ei_linked_point_t *linked_point)
{
	while (linked_point != NULL) {
		ei_linked_point_t *bye = linked_point;
		linked_point = linked_point->next;
		free(bye);
	}
}
