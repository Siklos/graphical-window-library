/**
 *  @file	draw_utils.c
 *
 *  @brief	Utils for drawing a pixel into a surface.
 *
 */

#include <stdlib.h>
#include "ei_types.h"
#include "hw_interface.h"
#include "ei_draw.h"

/**
 * \brief			Returns the coresponding color from a pixel.
 *
 * @param	surface		The surface to get its color indexes.
 *
 * @param	color		The buffer representing a color.
 *
 * @return	ei_color_t	Return the struct ei_color_t.
 */
ei_color_t buffer_to_color(ei_surface_t surface, uint32_t color)
{

	int ir, ig, ib, ia;
	hw_surface_get_channel_indices(surface, &ir, &ig, &ib, &ia);
	uint8_t red = color >> (8 * ir) & 0xff;
	uint8_t green = color >> (8 * ig) & 0xff;
	uint8_t blue = color >> (8 * ib) & 0xff;
	uint8_t alpha = color >> (8 * ia) & 0xff;
	ei_color_t res = { red, green, blue, alpha };
	return res;
}

/**
 * \brief			Returns the coresponding color from the coordinates of a pixel.
 *
 * @param	surface		The surface to get its color indexes.
 *
 * @param	x		The x position of the pixel.
 *
 * @param	y		The y position of the pixel.
 *
 * @return	ei_color_t	Return the struct ei_color_t.
 */
ei_color_t get_color(ei_surface_t surface, int x, int y)
{

	uint32_t *pixel_ptr = (uint32_t *) hw_surface_get_buffer(surface);
	ei_size_t size = hw_surface_get_size(surface);
	uint32_t color = pixel_ptr[x + size.width * y];
	return buffer_to_color(surface, color);
}

/**
 * \brief			Returns a color buffer corresponding to color indexes pixel.
 *
 * @param	surface		The surface to get its color indexes.
 *
 * @param	color		A color to get its buffer value.
 *
 * @return	uint32_t		Return the color buffer.
 */
uint32_t ei_map_rgba(ei_surface_t surface, const ei_color_t * color)
{
	int ir, ig, ib, ia;
	hw_surface_get_channel_indices(surface, &ir, &ig, &ib, &ia);
	return (color->red << (8 * ir)) + (color->green << (8 * ig))
	    + (color->blue << (8 * ib)) + (color->alpha << (8 * ia));
}

/**
 * \brief			Returns the coresponding buffer color from a color according to the color indexes of the x, y of a given surface.
 *
 * @param	surface		The surface to get its color indexes.
 *
 * @param	x		The x position of a pixel.
 *
 * @param	y		The y position of a pixel.
 *
 * @param	color		A color to get its buffer value.
 *
 * @return	uint32_t		Return the color buffer.
 */
uint32_t get_map_rgba(ei_surface_t surface, int x, int y,
		      const ei_color_t * color)
{
	if (color->alpha == 0xff) {
		return ei_map_rgba(surface, color);
	}
	if (color->alpha == 0x00) {
		uint32_t *pixel_ptr =
		    (uint32_t *) hw_surface_get_buffer(surface);
		ei_size_t size = hw_surface_get_size(surface);
		return pixel_ptr[x + size.width * y];
	}

	ei_color_t previous_color = get_color(surface, x, y);
	uint8_t red =
	    (color->alpha * color->red +
	     (255 - color->alpha) * previous_color.red) / 255;
	uint8_t green =
	    (color->alpha * color->green +
	     (255 - color->alpha) * previous_color.green) / 255;
	uint8_t blue =
	    (color->alpha * color->blue +
	     (255 - color->alpha) * previous_color.blue) / 255;
	ei_color_t new_color = { red, green, blue, previous_color.alpha };
	return ei_map_rgba(surface, &new_color);
}

/**
 * \brief			Checks if the (x, y) is in a given clipper.
 *
 * @param	x		The x position of a pixel.
 *
 * @param	y		The y position of a pixel.
 *
 * @param	clipper		The clipper of reference.
 *
 * @return	ei_bool_t	Return EI_TRUE if (x, y) is in the clipper, return EI_FALSE otherwise.
 */
ei_bool_t is_in_clipper(int x, int y, const ei_rect_t * clipper)
{
	if (clipper == NULL)
		return EI_TRUE;
	return (x >= clipper->top_left.x && y >= clipper->top_left.y &&
		x < (clipper->top_left.x + clipper->size.width) &&
		y < (clipper->top_left.y + clipper->size.height))
	    ? EI_TRUE : EI_FALSE;
}

/**
 * \brief			Draws the pixel at the coordinates (x,y) if allowed.
 *
 * @param	surface		Where to draw the pixel.
 *
 * @param	x		Coordinate of the pixel on the x_axis.
 *
 * @param	y		Coordinate of the pixel on the y_axis.
 *
 * @param	color		The pixel's color.
 *
 * @param	clipper		Can be NULL, in this case, draw on the entire window.
 *				If not NULL, the drawing is restricted within this rectangle.
 */
void draw_pixel(ei_surface_t surface, int x, int y, const ei_color_t * color,
		const ei_rect_t * clipper)
{
	if (clipper != NULL &&
	    !is_in_clipper(x, y, clipper))
		return;

	uint32_t *pixel_ptr = (uint32_t *) hw_surface_get_buffer(surface);
	ei_rect_t rect = hw_surface_get_rect(surface);
	ei_size_t size = rect.size;
	if (x < rect.top_left.x || y < rect.top_left.y ||
	    x >= rect.top_left.x + size.width || y >= rect.top_left.y + size.height)
		return;
	uint32_t rgba = get_map_rgba(surface, x, y, color);
	pixel_ptr[x + size.width * y] = rgba;
}
