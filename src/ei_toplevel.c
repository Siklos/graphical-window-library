/**
 *  @file	ei_toplevel.c
 *  @brief	Definition and registration of toplevel class.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ei_toplevel.h"
#include "ei_widgetclass.h"
#include "ei_utils.h"
#include "ei_event.h"
#include "ei_types.h"
#include "ei_application.h"

#include "macro.h"
#include "button_utils.h"
#include "widget_utils.h"

#define TITLE_BAR_HEIGHT 30
#define CORNER_SIZE 10
#define CLOSE_SIZE 10
#define RADIUS_CLOSE 4
#define OFFSET 5
#define DEFAULT_TITLE "Toplevel"

static const ei_color_t ei_default_titlebar_color = { 0x88, 0x88, 0x88, 0xdd };
static const ei_color_t ei_default_close_color = { 0xff, 0x00, 0x00, 0xff };

/**
 * \brief	A structure that stores information about the toplevel class.
 */
ei_widgetclass_t toplevel_class = { "toplevel",
				    ei_toplevel_allocfunc,
				    ei_toplevel_releasefunc,
				    ei_toplevel_drawfunc,
				    ei_toplevel_setdefaultsfunc,
				    ei_toplevel_geomnotifyfunc,
				    ei_toplevel_handlefunc,
				    NULL };

/**
 * \brief	A function that allocates a block of memory that is big enough to store the
 *		attributes of a widget of a class. After allocation, the function *must*
 *		initialize the memory to 0.
 *
 * @return		A block of memory with all bytes set to 0.
 */
void *ei_toplevel_allocfunc()
{
	ei_toplevel_widget_t *ei_toplevel_widget =
		calloc(1, sizeof(ei_toplevel_widget_t));
	return ei_toplevel_widget;
}

/**
 * \brief	A function that releases the memory used by a widget before it is destroyed.
 *		The \ref ei_widget_t structure itself, passed as parameter, must *not* be freed by
 *		these functions. Can be set to NULL in \ref ei_widgetclass_t if no memory is used by
 *		a class of widget.
 *
 * @param	widget		The widget which resources are to be freed.
 */
void ei_toplevel_releasefunc(struct ei_widget_t *widget)
{
	if (widget == NULL)
		return;

	ei_toplevel_widget_t *toplevel = (ei_toplevel_widget_t *)widget;
	if(toplevel->title != NULL) {
		free(toplevel->title);
	}

	ei_widget_extended_t *widget_extended = (ei_widget_extended_t *)widget;

	if (widget_extended->destroy_cb != NULL) {
		widget_extended->destroy_cb(widget, NULL,
					    widget_extended->destroy_cb_param);
	}

	widget->wclass = NULL;
}

/**
 * \brief	A function that draws widgets of a class.
 *
 * @param	widget		A pointer to the widget instance to draw.
 * @param	surface		Where to draw the widget. The actual location of the widget in the
 *				surface is stored in its "screen_location" field.
 * @param	pick_surface	Where to draw the pick version of the widget.
 * @param	clipper		If not NULL, the drawing is restricted within this rectangle
 *				(expressed in the surface reference toplevel).
 */
void ei_toplevel_drawfunc(struct ei_widget_t *widget, ei_surface_t surface,
			  ei_surface_t pick_surface, ei_rect_t *clipper)
{
	if ((clipper != NULL && clipper->size.width == 0 && clipper->size.height == 0) ||
	    (widget->screen_location.size.height == 0 && widget->screen_location.size.width == 0))
		return;

	ei_toplevel_widget_t *toplevel = (ei_toplevel_widget_t *)widget;

	hw_surface_lock(surface);

	/* la taille sans l'encart du haut mais avec les décorations */
	ei_size_t toplevel_size = ei_size(widget->screen_location.size.width + (toplevel->border_width << 1),
					  widget->screen_location.size.height + toplevel->border_width);

	/* On commence par dessiner l'encart du haut */
	ei_rect_t titlebar_clipper;
	titlebar_clipper.top_left = ei_point(widget->screen_location.top_left.x - toplevel->border_width,
					     widget->screen_location.top_left.y - TITLE_BAR_HEIGHT);
	titlebar_clipper.size.width = toplevel_size.width;
	titlebar_clipper.size.height = TITLE_BAR_HEIGHT;

	int radius_angle = 10;
	draw_toplevel(surface, ei_default_titlebar_color, titlebar_clipper, radius_angle, clipper);

	ei_point_t where_text = titlebar_clipper.top_left;
	where_text.x += OFFSET;

	/* On dessine le carré de fermeture de la toplevel */
	if (toplevel->closable) {
		ei_rect_t close_clipper;
		close_clipper.top_left.x = titlebar_clipper.top_left.x + CLOSE_SIZE;
		close_clipper.top_left.y = titlebar_clipper.top_left.y + CLOSE_SIZE;
		close_clipper.size = ei_size(CLOSE_SIZE, CLOSE_SIZE);
		where_text.x += (CLOSE_SIZE << 1) + OFFSET;

		int radius_angle = RADIUS_CLOSE;
		ei_linked_point_t *close = NULL;
		pick_button(surface, ei_default_close_color, close, close_clipper, radius_angle, clipper);
	}

	ei_size_t text_size;
	hw_text_compute_size(toplevel->title, ei_default_font,
			     &text_size.width, &text_size.height);

	ei_rect_t text_rect = ei_rect(where_text,
		ei_size( MIN(text_size.width, titlebar_clipper.size.width - CORNER_SIZE -
			(where_text.x - titlebar_clipper.top_left.x)), text_size.height));

	ei_rect_t true_clipper_text = clip_clipper(text_rect, clipper);

	ei_draw_text(surface, &where_text, toplevel->title, ei_default_font,
		     &ei_font_default_color, &true_clipper_text);


	/* Dessin des bordures */
	if (toplevel->border_width != 0) {
		ei_rect_t border_clipper;
		/* border left */
		border_clipper.top_left = widget->screen_location.top_left;
		border_clipper.top_left.x -= toplevel->border_width;
		border_clipper.size.width = toplevel->border_width;
		border_clipper.size.height = widget->screen_location.size.height + toplevel->border_width;

		ei_rect_t true_clipper_left = clip_clipper(border_clipper, clipper);

		ei_fill(surface, &ei_default_titlebar_color, &true_clipper_left);

		/* border right */
		border_clipper.top_left.x += widget->screen_location.size.width + toplevel->border_width;

		ei_rect_t true_clipper_right = clip_clipper(border_clipper, clipper);

		ei_fill(surface, &ei_default_titlebar_color, &true_clipper_right);

		/* border bottom */
		border_clipper.top_left.x = widget->screen_location.top_left.x - toplevel->border_width;
		border_clipper.top_left.y += widget->screen_location.size.height;
		border_clipper.size.width = widget->screen_location.size.width + (toplevel->border_width << 1);
		border_clipper.size.height = toplevel->border_width;

		ei_rect_t true_clipper_down = clip_clipper(border_clipper, clipper);

		ei_fill(surface, &ei_default_titlebar_color, &true_clipper_down);
	}

	/* On dessine la partie intérieure du rectangle */
	ei_rect_t content_clipper;
	content_clipper.top_left.x = widget->screen_location.top_left.x;
	content_clipper.top_left.y = widget->screen_location.top_left.y;
	content_clipper.size = widget->screen_location.size;

	ei_rect_t true_clipper = clip_clipper(content_clipper, clipper);

	ei_fill(surface, &(toplevel->color), &true_clipper);

	/* On dessine dans la surface de sélection du widget concerné (pick_surface) */
	ei_rect_t screen_location;
	ei_point_t top_left = ei_point(widget->screen_location.top_left.x - toplevel->border_width,
				       widget->screen_location.top_left.y - TITLE_BAR_HEIGHT);
	screen_location.top_left = top_left;
	screen_location.size = ei_size(toplevel_size.width, toplevel_size.height + TITLE_BAR_HEIGHT);

	ei_rect_t true_screen_pick = clip_clipper(screen_location, clipper);

	hw_surface_lock(pick_surface);
	ei_fill(pick_surface, widget->pick_color, &true_screen_pick);
	hw_surface_unlock(pick_surface);

	hw_surface_unlock(surface);

	draw_children(widget, surface, pick_surface);

	if (toplevel->resizable) {
		hw_surface_lock(surface);
		ei_rect_t resize_clip;
		resize_clip.top_left.x = widget->screen_location.top_left.x +
					 toplevel_size.width - CORNER_SIZE -
					 toplevel->border_width;
		resize_clip.top_left.y = widget->screen_location.top_left.y +
					 widget->screen_location.size.height -
					 CORNER_SIZE;
		resize_clip.size = ei_size(CORNER_SIZE, CORNER_SIZE);

		ei_rect_t true_resize = clip_clipper(resize_clip, clipper);

		ei_fill(surface, &ei_default_titlebar_color, &true_resize);
		hw_surface_unlock(surface);
	}
}

/**
 * \brief	A function that sets the default values for a class.
 *
 * @param	widget		A pointer to the widget instance to intialize.
 */
void ei_toplevel_setdefaultsfunc(struct ei_widget_t *widget)
{
	ei_toplevel_widget_t *toplevel = (ei_toplevel_widget_t *)widget;
	widget->requested_size = ei_size(320, 240);
	toplevel->color = ei_default_background_color;
	toplevel->border_width = 4;
	toplevel->title = malloc(sizeof(char) * strlen(DEFAULT_TITLE) + 1);
	strcpy(toplevel->title, DEFAULT_TITLE);
	toplevel->closable = EI_TRUE;
	toplevel->resizable = ei_axis_both;
	toplevel->min_size = ei_size(160, 120);
	toplevel->state = S_WAIT;
}

/**
 * \brief 	A function that is called to notify the widget that its geometry has been modified
 *		by its geometry manager. Can set to NULL in \ref ei_widgetclass_t.
 *
 * @param	widget		The widget instance to notify of a geometry change.
 * @param	rect		The new rectangular screen location of the widget
 *				(i.e. = widget->screen_location).
 */
void ei_toplevel_geomnotifyfunc(struct ei_widget_t *widget, ei_rect_t rect)
{
	ei_toplevel_widget_t *toplevel = (ei_toplevel_widget_t *)widget;
	const ei_rect_t wrect = widget->screen_location;

	ei_rect_t new_rect = widget->screen_location;

	if (toplevel->state == S_MOVE) {
		new_rect.top_left =
			ei_point(MIN(wrect.top_left.x, rect.top_left.x) -
					 toplevel->border_width,
				 MIN(wrect.top_left.y, rect.top_left.y) -
					 TITLE_BAR_HEIGHT);

		/* coin en bas à droite avec la bordure */
		ei_point_t bottom_right =
			ei_point(MAX(wrect.top_left.x + wrect.size.width,
				     rect.top_left.x + rect.size.width) +
					 toplevel->border_width,
				 MAX(wrect.top_left.y + wrect.size.height,
				     rect.top_left.y + rect.size.height) +
					 toplevel->border_width);

		new_rect.size = ei_size(bottom_right.x - new_rect.top_left.x,
					bottom_right.y - new_rect.top_left.y);

	} else if (toplevel->state == S_RESIZE) {
		new_rect.top_left.x -= toplevel->border_width;
		new_rect.top_left.y -= TITLE_BAR_HEIGHT;
		new_rect.size.width = MAX(wrect.size.width, rect.size.width) +
				      (toplevel->border_width << 1);
		new_rect.size.height =
			MAX(wrect.size.height, rect.size.height) +
			toplevel->border_width + TITLE_BAR_HEIGHT;

	} else {
		new_rect.top_left.x -= toplevel->border_width;
		new_rect.top_left.y -= TITLE_BAR_HEIGHT;
		new_rect.size.width += toplevel->border_width << 1;
		new_rect.size.height +=
			toplevel->border_width + TITLE_BAR_HEIGHT;
	}

	ei_app_invalidate_rect(&new_rect);
	widget->screen_location = rect;
}

/**
 * @brief	A function that is called in response to an event. This function
 *		is internal to the library. It implements the generic behavior of
 *		a widget (for example a button looks sunken when clicked)
 *
 * @param	widget		The widget for which the event was generated.
 * @param	event		The event containing all its parameters (type, etc.)
 *
 * @return			A boolean telling if the event was consumed by the callback or not.
 *				If TRUE, the library does not try to call other callbacks for this
 *				event. If FALSE, the library will call the next callback registered
 *				for this event, if any.
 *				Note: The callback may execute many operations and still return
 *				FALSE, or return TRUE without having done anything.
 */
ei_bool_t ei_toplevel_handlefunc(struct ei_widget_t *widget,
				 struct ei_event_t *event)
{
	ei_toplevel_widget_t *toplevel = (ei_toplevel_widget_t *)widget;

	if (toplevel->state == S_DEAD) {
		ei_event_set_active_widget(NULL);
		ei_widget_destroy(widget);
		return EI_TRUE;
	}

	ei_point_t where = event->param.mouse.where;
	ei_rect_t rect = widget->screen_location;

	switch (event->type) {

	case ei_ev_mouse_buttondown:

		if (widget->parent != NULL &&
		    widget != widget->parent->children_tail) {
			toplevel->state = S_FRONT;
			set_in_front(widget);
			ei_toplevel_geomnotifyfunc(widget, rect);
			toplevel->state = S_WAIT;
			return EI_TRUE;
		}

		if (where.x >=
			    rect.top_left.x - toplevel->border_width + CLOSE_SIZE &&
		    where.y >=
			    rect.top_left.y - TITLE_BAR_HEIGHT + CLOSE_SIZE &&
		    where.x <= rect.top_left.x - toplevel->border_width +
				       CLOSE_SIZE + (RADIUS_CLOSE << 1) &&
		    where.y <= rect.top_left.y - TITLE_BAR_HEIGHT +
				       CLOSE_SIZE + (RADIUS_CLOSE << 1)) {
			ei_rect_t empty_rect = ei_rect_zero();
			if (widget->placer_params != NULL) {
				free(widget->placer_params);
				widget->placer_params = NULL;
			}
			toplevel->state = S_DEAD;
			widget->wclass->geomnotifyfunc(widget, empty_rect);
			ei_event_set_active_widget(widget);
			return EI_TRUE;

		} else if (toplevel->resizable != ei_axis_none &&
			   (where.x >= rect.top_left.x + rect.size.width -
					       CORNER_SIZE &&
			    where.y >= rect.top_left.y + rect.size.height -
					       CORNER_SIZE &&
			    where.y <= rect.top_left.y + rect.size.height +
					       toplevel->border_width &&
			    where.x <= rect.top_left.x + rect.size.width +
					       toplevel->border_width)) {
			toplevel->state = S_RESIZE;
			ei_event_set_active_widget(widget);
			return EI_TRUE;

		} else if (where.x >=
				   rect.top_left.x - toplevel->border_width &&
			   where.y >= rect.top_left.y - TITLE_BAR_HEIGHT &&
			   where.y <= rect.top_left.y &&
			   where.x <= rect.top_left.x + rect.size.width +
					      toplevel->border_width) {
			toplevel->state = S_MOVE;
			toplevel->stored_distance =
				ei_size(where.x - rect.top_left.x,
					rect.top_left.y - where.y);
			ei_event_set_active_widget(widget);
			return EI_TRUE;
		}
		break;

	case ei_ev_mouse_move:

		if (widget->placer_params != NULL) {
			free(widget->placer_params);
			widget->placer_params = NULL;
		}

		if (ei_event_get_active_widget() == widget) {

			if (toplevel->state == S_MOVE) {
				/* Deplace */

				ei_point_t top_left = ei_point(
					where.x - (toplevel->stored_distance
							   .width),
					where.y + (toplevel->stored_distance
							   .height)
				);

				ei_rect_t new_rect = { top_left, rect.size };
				widget->wclass->geomnotifyfunc(widget,
							       new_rect);
				return EI_TRUE;
			}

			if (toplevel->state == S_RESIZE) {
				ei_rect_t new_rect = {
					rect.top_left,
					{ MAX(where.x - rect.top_left.x,
					      toplevel->min_size.width),
					  MAX(where.y - rect.top_left.y,
					      toplevel->min_size.height) }
				};

				if (toplevel->resizable == ei_axis_y) {
					new_rect.size.width = rect.size.width;
				} else if (toplevel->resizable == ei_axis_x) {
					new_rect.size.height = rect.size.height;
				}
				widget->wclass->geomnotifyfunc(widget,
							       new_rect);
				return EI_TRUE;
			}
		}
		break;

	case ei_ev_mouse_buttonup:

		if (ei_event_get_active_widget() == widget) {

			if (toplevel->state == S_RESIZE) {

				ei_rect_t new_rect = {
					rect.top_left,
					{ MAX(where.x - rect.top_left.x,
					      toplevel->min_size.width),
					  MAX(where.y - rect.top_left.y,
					      toplevel->min_size.height) }
				};

				if (toplevel->resizable == ei_axis_y) {
					new_rect.size.width = rect.size.width;
				} else if (toplevel->resizable == ei_axis_x) {
					new_rect.size.height = rect.size.height;
				}
				if (widget->placer_params != NULL) {
					free(widget->placer_params);
					widget->placer_params = NULL;
				}
				widget->wclass->geomnotifyfunc(widget,
							       new_rect);
				ei_event_set_active_widget(NULL);
				toplevel->state = S_WAIT;
				return EI_TRUE;

			} else if (toplevel->state == S_MOVE) {

				/* Deplace */
				ei_point_t top_left = {
					where.x - (toplevel->stored_distance
							   .width),
					where.y + (toplevel->stored_distance
							   .height)
				};

				ei_rect_t new_rect = { top_left, rect.size };
				if (widget->placer_params != NULL) {
					free(widget->placer_params);
					widget->placer_params = NULL;
				}
				widget->wclass->geomnotifyfunc(widget,
							       new_rect);
				ei_event_set_active_widget(NULL);
				toplevel->state = S_WAIT;
				return EI_TRUE;
			}
		}
		break;

	default:
		break;
	}
	return EI_FALSE;
}
