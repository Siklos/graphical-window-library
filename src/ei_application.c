/**
 *  @file	ei_application.c
 *  @brief	Manages the main steps of a graphical application: initialization, main window,
 *		main loop, quitting, resource freeing.
 *
 */

#include <stdlib.h>
#include <stdio.h>

#include "ei_application.h"
#include "ei_event.h"
#include "ei_types.h"
#include "ei_widget.h"
#include "ei_widgetclass.h"
#include "ei_draw.c"
#include "hw_interface.h"

#define FRAME_SKIP 6

ei_surface_t main_window = NULL;
ei_widget_t *root_widget = NULL;
ei_surface_t pick_surface = NULL;
ei_bool_t program_not_over = EI_TRUE;

ei_linked_rect_t *rect_list;

/**
 * \brief 	Free a linked list of rects
 * 
 */
void free_rect_list();

/**
 * \brief 	Refresh the main_window and the pick_surface
 * 
 */
void refresh_drawings();

/**
 * \brief	Creates an application.
 *		<ul>
 *			<li> initializes the hardware (calls \ref hw_init), </li>
 *			<li> registers all classes of widget and all geometry managers, </li>
 *			<li> creates the root window (either in a system window, or the entire
 *				screen), </li>
 *			<li> creates the root widget to accress the root window. </li>
 *		</ul>
 *
 * @param	main_window_size	If "fullscreen is false, the size of the root window of the
 *					application.
 *					If "fullscreen" is true, the current monitor resolution is
 *					used as the size of the root window, this size is returned
 *					in this parameter.
 * @param	fullscreen		If true, the root window is the entire screen. Otherwise, it
 *					is a system window.
 */
void ei_app_create(ei_size_t *main_window_size, ei_bool_t fullscreen)
{
	hw_init();

	ei_frame_register_class();
	ei_button_register_class();
	ei_toplevel_register_class();

	main_window = hw_create_window(main_window_size, fullscreen);
	pick_surface =
		hw_surface_create(main_window, main_window_size, EI_FALSE);
	root_widget = ei_widget_create("frame", NULL);
	ei_frame_configure(ei_app_root_widget(), main_window_size, NULL, NULL,
			   NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
	ei_place(ei_app_root_widget(), NULL, NULL, NULL, NULL, NULL, NULL, NULL,
		 NULL, NULL);
}

/**
 * \brief	Releases all the resources of the application, and releases the hardware
 *		(ie. calls \ref hw_quit).
 */
void ei_app_free()
{
	ei_widget_destroy(root_widget);
	hw_surface_free(main_window);
	hw_quit();
}

/**
 * \brief	Runs the application: enters the main event loop. Exits when
 *		\ref ei_app_quit_request is called.
 */
void ei_app_run()
{

	ei_placer_run(root_widget);
	root_widget->wclass->drawfunc(root_widget, main_window, pick_surface,
				      &root_widget->screen_location);
	ei_event_t *tmp_event = malloc(sizeof(ei_event_t));
	hw_surface_update_rects(main_window, NULL);
	int frame_timing = 1;
	while (program_not_over) {
		if (rect_list != NULL) {
			refresh_drawings();
			hw_surface_update_rects(main_window, rect_list);
			free_rect_list();
		}
		hw_event_wait_next(tmp_event);
		ei_widget_t *concerned_widget = NULL;
		ei_eventtype_t event_type = tmp_event->type;
		if (event_type == ei_ev_mouse_move &&
		    frame_timing++ < FRAME_SKIP) {
			continue;
		}
		frame_timing = 1;
		if (ei_event_get_active_widget() == NULL &&
			    (event_type == ei_ev_mouse_buttondown ||
		    event_type == ei_ev_mouse_buttonup ||
		    event_type == ei_ev_mouse_move)) {
			concerned_widget =
				ei_widget_pick(&tmp_event->param.mouse.where);
		}
		if (concerned_widget == NULL &&
		    ei_event_get_active_widget() == NULL) {
			ei_default_handle_func_t func =
				ei_event_get_default_handle_func();
			if (func == NULL)
				continue;
			func(tmp_event);
			continue;
		}

		if (ei_event_get_active_widget() != NULL &&
		    ei_event_get_active_widget()->wclass->handlefunc != NULL) {
			ei_event_get_active_widget()->wclass->handlefunc(
				ei_event_get_active_widget(), tmp_event);
		} else if (concerned_widget != NULL && concerned_widget->wclass->handlefunc != NULL) {
			concerned_widget->wclass->handlefunc(concerned_widget,
							     tmp_event);
		}
	}
	free(tmp_event);
}

/**
 * \brief	Adds a rectangle to the list of rectangles that must be updated on screen. The real
 *		update on the screen will be done at the right moment in the main loop.
 *
 * @param	rect		The rectangle to add, expressed in the root window coordinates.
 *				A copy is made, so it is safe to release the rectangle on return.
 */
void ei_app_invalidate_rect(ei_rect_t *rect)
{
	ei_rect_t surf_rect = hw_surface_get_rect(main_window);

	ei_linked_rect_t *new_rect_list_node = (ei_linked_rect_t *) malloc(sizeof(ei_linked_rect_t));
	new_rect_list_node->rect = *rect;
	new_rect_list_node->next = NULL;

	if (new_rect_list_node->rect.top_left.x > surf_rect.size.width ||
	    new_rect_list_node->rect.top_left.y > surf_rect.size.height ||
	    new_rect_list_node->rect.top_left.x + new_rect_list_node->rect.size.width < 0 ||
	    new_rect_list_node->rect.top_left.y + new_rect_list_node->rect.size.height < 0) {
		free(new_rect_list_node);
		return;
	}

	if (new_rect_list_node->rect.top_left.x < 0)
	{
		new_rect_list_node->rect.size.width += new_rect_list_node->rect.top_left.x;
		new_rect_list_node->rect.top_left.x = 0;
	}

	if (new_rect_list_node->rect.top_left.y < 0) {
		new_rect_list_node->rect.size.height += new_rect_list_node->rect.top_left.y;
		new_rect_list_node->rect.top_left.y = 0;
	}

	if (new_rect_list_node->rect.top_left.x + new_rect_list_node->rect.size.width > surf_rect.size.width) {
		new_rect_list_node->rect.size.width -=
			((new_rect_list_node->rect.top_left.x + new_rect_list_node->rect.size.width) -
				surf_rect.size.width);
	}

	if (new_rect_list_node->rect.top_left.y + new_rect_list_node->rect.size.height > surf_rect.size.height) {
		if (new_rect_list_node->rect.top_left.y) {

		}
		new_rect_list_node->rect.size.height -=
			((new_rect_list_node->rect.top_left.y + new_rect_list_node->rect.size.height) - surf_rect.size.height);
	}



	if (rect_list == NULL) {
		rect_list = new_rect_list_node;
		return;
	}
	new_rect_list_node->next = rect_list;
	rect_list = new_rect_list_node;
}

/**
 * \brief	Tells the application to quite. Is usually called by an event handler (for example
 *		when pressing the "Escape" key).
 */
void ei_app_quit_request()
{
	program_not_over = EI_FALSE;
}

/**
 * \brief	Returns the "root widget" of the application: a "frame" widget that encapsulate the
 *		root window.
 *
 * @return 			The root widget.
 */
ei_widget_t *ei_app_root_widget()
{
	return root_widget;
}

/**
 * \brief	Returns the surface of the root window. Used to create surfaces with similar r, g, b
 *		channels.
 *
 * @return 			The surface of the root window.
 */
ei_surface_t ei_app_root_surface()
{
	return main_window;
}

/**
 * \brief 	Free a linked list of rects
 * 
 */
void free_rect_list()
{
	while(rect_list != NULL) {
		ei_linked_rect_t * tmp = rect_list;
		rect_list = rect_list->next;
		free(tmp);
	}
	rect_list = NULL;
}

/**
 * \brief 	Refresh the main_window and the pick_surface
 * 
 */
void refresh_drawings() {
	ei_linked_rect_t * rect_node = rect_list;
	while (rect_node != NULL) {
		root_widget->wclass->drawfunc(root_widget, main_window,
					      pick_surface, &(rect_node->rect));
		rect_node = rect_node->next;
	}
}
