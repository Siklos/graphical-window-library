/**
 *  @file	widget_utils.c
 *  @brief	Utils for a widget.
 *
 */

#include <stdint.h>
#include <stdlib.h>
#include <math.h>

#include "ei_types.h"
#include "ei_utils.h"
#include "ei_widget.h"
#include "hw_interface.h"

#include "macro.h"

/**
* \brief			Returns the lighten version of the color set in parameter.
*
* @param	color		The color to lighten.
*
* @return			The lighten version of the color.
*/
ei_color_t lighten_color(ei_color_t color)
{
	unsigned char red = (uint8_t)sqrt(255 * color.red);
	unsigned char green = (uint8_t)sqrt(255 * color.green);
	unsigned char blue = (uint8_t)sqrt(255 * color.blue);
	ei_color_t l_color = { red, green, blue, color.alpha };
	return l_color;
}

/**
* \brief			Returns the darken version of the color set in parameter.
*
* @param	color		The color to darken.
*
* @return			The darken version of the color.
*/
ei_color_t darken_color(ei_color_t color)
{
	unsigned char red = (uint8_t)((color.red * color.red) / 255);
	unsigned char green = (uint8_t)((color.green * color.green) / 255);
	unsigned char blue = (uint8_t)((color.blue * color.blue) / 255);
	ei_color_t d_color = { red, green, blue, color.alpha };
	return d_color;
}

/**
 * \brief			Applies and returns a color with the gamma applied
 *				to.
 *
 * @param	color		The color to apply the gamma.
 *
 * @param	gamma		The value of the gamma.
 *
 * @return			A new color with the gamma applied to.
 */
ei_color_t color_plus(ei_color_t color, float gamma)
{
	float a = (uint8_t)((color.red + color.green + color.blue) / 3);
	float a_barre = (uint8_t)(255 * exp(gamma * log(a / 255)));
	float r_barre = color.red + a_barre - a;
	float g_barre = color.green + a_barre - a;
	float b_barre = color.blue + a_barre - a;
	unsigned char red = MAX(0, MIN(255, r_barre));
	unsigned char green = MAX(0, MIN(255, g_barre));
	unsigned char blue = MAX(0, MIN(255, b_barre));
	ei_color_t d_color = { red, green, blue, color.alpha };
	return d_color;
}

/**
 * \brief			Attaches an anchor to a rectangle. Return a new
 *				value for the top_left of the rectangle to
 *				reposition.
 *
 * @param	anchor		A anchor to attach to the widget.
 *
 * @param	rect		A rect to attach the anchor.
 *
 * @return			A point corresponding to the new top_left of the
 *				rectangle.
 */
ei_point_t apply_anchor(ei_anchor_t anchor, ei_rect_t rect)
{
	ei_point_t point = { rect.top_left.x, rect.top_left.y };
	switch (anchor) {
	case ei_anc_east:
	case ei_anc_northeast:
	case ei_anc_southeast:
		point.x = rect.top_left.x - rect.size.width;
		break;
	case ei_anc_north:
	case ei_anc_south:
	case ei_anc_center:
		point.x = rect.top_left.x - rect.size.width / 2;
		break;
	default:
		break;
	}

	switch (anchor) {
	case ei_anc_south:
	case ei_anc_southwest:
	case ei_anc_southeast:
		point.y = rect.top_left.y - rect.size.height;
		break;
	case ei_anc_west:
	case ei_anc_east:
	case ei_anc_center:
		point.y = rect.top_left.y - rect.size.height / 2;
		break;
	default:
		break;
	}
	return point;
}

/**
 * @brief			Computes the anchor of the image, i.e. where it is placed within the widget
 *				when the size of the widget is bigger than the size of the image.
 *
 * @param	anchor		How to anchor the widget to the position of the parent.
 *
 * @param	parent		the rectangular form of the parent in the root surface.
 *
 * @param	img_text_size	Size of the text/image.
 *
 * @param	corner_radius	(Optional) Shift the point if a corner is
 *				different from 0.
 *
 * @return			A point aligned from the anchor of the parent.
 */
ei_point_t position_from_anchor(ei_anchor_t anchor, ei_rect_t parent,
				ei_size_t img_text_size, int corner_radius)
{
	ei_point_t top_left_text_img = parent.top_left;
	int top_corner = (int)(0.3 * corner_radius);

	switch (anchor) {
	case ei_anc_northwest:
		top_left_text_img.x += top_corner;
		top_left_text_img.y += top_corner;
		break;
	case ei_anc_north:
		top_left_text_img.x +=
			parent.size.width / 2 - img_text_size.width / 2;
		break;
	case ei_anc_northeast:
		top_left_text_img.x +=
			parent.size.width - img_text_size.width - top_corner;
		top_left_text_img.y += top_corner;
		break;
	case ei_anc_west:
		top_left_text_img.y +=
			parent.size.height / 2 - img_text_size.height / 2;
		break;
	case ei_anc_center:
		top_left_text_img.x +=
			parent.size.width / 2 - img_text_size.width / 2;
		top_left_text_img.y +=
			parent.size.height / 2 - img_text_size.height / 2;
		break;
	case ei_anc_east:
		top_left_text_img.x += parent.size.width - img_text_size.width;
		top_left_text_img.y +=
			parent.size.height / 2 - img_text_size.height / 2;
		break;
	case ei_anc_southwest:
		top_left_text_img.x += top_corner;
		top_left_text_img.y +=
			parent.size.height - img_text_size.height - top_corner;
		break;
	case ei_anc_south:
		top_left_text_img.x +=
			parent.size.width / 2 - img_text_size.width / 2;
		top_left_text_img.y +=
			parent.size.height - img_text_size.height;
		break;
	case ei_anc_southeast:
		top_left_text_img.x +=
			parent.size.width - img_text_size.width - top_corner;
		top_left_text_img.y +=
			parent.size.height - img_text_size.height - top_corner;
		break;
	default:
		break;
	}

	if (top_left_text_img.x < parent.top_left.x)
		top_left_text_img.x = parent.top_left.x;
	if (top_left_text_img.y < parent.top_left.y)
		top_left_text_img.y = parent.top_left.y;

	return top_left_text_img;
}

/**
 * @brief			Draws the children of the widget.
 *
 * @param	widget		A the parent widget to draw its children.
 *
 * @param	surface		The surface to draw into.
 *
 * @param	pick_surface	The pick surface to draw the pick_colors into.
 *
 */
void draw_children(ei_widget_t *widget, ei_surface_t surface,
		   ei_surface_t pick_surface)
{
	ei_widget_t *head = widget->children_head;
	while (head != NULL) {
		ei_placer_run(head);
		head->wclass->drawfunc(head, surface, pick_surface,
				       widget->content_rect);
		head = head->next_sibling;
	}
}

/**
 * \brief			Sets the widget to the front of the widgets within the parent.
 *
 * @param	widget		The widget to set to the front.
 *
 */
void set_in_front(ei_widget_t *widget)
{
	if (widget != widget->parent->children_head) {
		ei_widget_t *prec = widget->parent->children_head;
		while (prec->next_sibling != widget) {
			prec = prec->next_sibling;
		}
		prec->next_sibling = widget->next_sibling;
	} else {
		widget->parent->children_head = widget->next_sibling;
	}
	widget->next_sibling = NULL;
	widget->parent->children_tail->next_sibling = widget;
	widget->parent->children_tail = widget;
}

/**
 * @brief			Inverts the relief sent in parameter.
 *
 * @param	actual		The relief to invert.
 *
 */
ei_relief_t inverse_relief(ei_relief_t actual)
{
	if (actual == ei_relief_none)
		return ei_relief_none;
	return (actual == ei_relief_raised) ? ei_relief_sunken :
					      ei_relief_raised;
}

ei_rect_t clip_clipper(ei_rect_t content, ei_rect_t *clipper)
{
	ei_rect_t new_clipper;

	if ((content.top_left.x >= clipper->top_left.x) &&
	    (content.top_left.x < clipper->top_left.x + clipper->size.width)) {

		new_clipper.top_left.x = content.top_left.x;
		new_clipper.size.width = MIN(content.size.width,
			clipper->top_left.x + clipper->size.width - content.top_left.x);

	} else if (content.top_left.x < clipper->top_left.x) {

		new_clipper.top_left.x = clipper->top_left.x;
		new_clipper.size.width = MIN( clipper->size.width,
		content.size.width - (clipper->top_left.x - content.top_left.x));

	} else {

		return ei_rect(ei_point_zero(), ei_size_zero());

	}

	if ((content.top_left.y >= clipper->top_left.y) &&
	    (content.top_left.y < clipper->top_left.y + clipper->size.height)) {

		new_clipper.top_left.y = content.top_left.y;
		new_clipper.size.height = MIN(content.size.height,
			clipper->top_left.y + clipper->size.height - content.top_left.y);

	} else if (content.top_left.y < clipper->top_left.y) {

		new_clipper.top_left.y = clipper->top_left.y;
		new_clipper.size.height = MIN( clipper->size.height,
		content.size.height - (clipper->top_left.y - content.top_left.y));

	} else {

		return ei_rect(ei_point_zero(), ei_size_zero());
	}

	return new_clipper;



}
