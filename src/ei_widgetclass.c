/**
 *  @file	ei_widgetclass.c
 *  @brief	Definition and registration of widget classes.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ei_widgetclass.h"
#include "ei_frame.h"
#include "ei_button.h"
#include "ei_toplevel.h"

#include "hw_interface.h"
#include "ei_draw.h"

ei_widgetclass_t *list_widgetclass;
extern ei_widgetclass_t frame_class;
extern ei_widgetclass_t button_class;
extern ei_widgetclass_t toplevel_class;

/**
 * @brief	Registers a class to the program so that widgets of this class can be created.
 *		This must be done only once per widged class in the application.
 *
 * @param	widgetclass	The structure describing the class.
 */
void ei_widgetclass_register(ei_widgetclass_t *widgetclass)
{
	if (list_widgetclass == NULL) {
		list_widgetclass = widgetclass;
		return;
	}
	ei_widgetclass_t **head = &list_widgetclass;
	widgetclass->next = *head;
	list_widgetclass = widgetclass;
}

/**
 * @brief	Returns the structure describing a class, from its name.
 *
 * @param	name		The name of the class of widget.
 *
 * @return			The structure describing the class.
 */
ei_widgetclass_t *ei_widgetclass_from_name(ei_widgetclass_name_t name)
{
	if (list_widgetclass == NULL)
		return NULL;
	ei_widgetclass_t *head = list_widgetclass;
	while (head != NULL) {
		if(strcmp(head->name, ei_widgetclass_stringname(name)) == 0) {
			break;
		}
		head = head->next;
	}
	return head;
}

/**
 * \brief	Registers the "frame" widget class in the program. This must be called only
 *		once before widgets of the class "frame" can be created and configured with
 *		\ref ei_frame_configure.
 */
void ei_frame_register_class()
{
	ei_widgetclass_register(&frame_class);
}

/**
 * \brief	Registers the "button" widget class in the program. This must be called only
 *		once before widgets of the class "button" can be created and configured with
 *		\ref ei_button_configure.
 */
void ei_button_register_class()
{
	ei_widgetclass_register(&button_class);
}

/**
 * \brief	Registers the "toplevel" widget class in the program. This must be called only
 *		once before widgets of the class "toplevel" can be created and configured with
 *		\ref ei_toplevel_configure.
 */
void ei_toplevel_register_class()
{
	ei_widgetclass_register(&toplevel_class);
}
