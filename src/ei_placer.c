/**
 *  @file	ei_placer.c
 *  @brief	Manages the positionning and sizing of widgets on the screen.
 *
 */

#include <stdlib.h>
#include <stdbool.h>

#include "ei_types.h"
#include "ei_widget.h"
#include "ei_placer.h"
#include "ei_utils.h"
#include "widget_utils.h"

/**
 * \brief	Configures the geometry of a widget using the "placer" geometry manager.
 * 		If the widget was already managed by another geometry manager, then it is first
 *		removed from the previous geometry manager.
 * 		If the widget was already managed by the "placer", then this calls simply updates
 *		the placer parameters: arguments that are not NULL replace previous values.
 * 		When the arguments are passed as NULL, the placer uses default values (detailed in
 *		the argument descriptions below). If no size is provided (either absolute or
 *		relative), then the requested size of the widget is used, i.e. the minimal size
 *		required to display its content.
 *
 * @param	widget		The widget to place.
 * @param	anchor		How to anchor the widget to the position defined by the placer
 *				(defaults to ei_anc_northwest).
 * @param	x		The absolute x position of the widget (defaults to 0).
 * @param	y		The absolute y position of the widget (defaults to 0).
 * @param	width		The absolute width for the widget (defaults to the requested width
 *				of the widget if rel_width is NULL, or 0 otherwise).
 * @param	height		The absolute height for the widget (defaults to the requested height
 *				of the widget if rel_height is NULL, or 0 otherwise).
 * @param	rel_x		The relative x position of the widget: 0.0 corresponds to the left
b *				side of the master, 1.0 to the right side (defaults to 0.0).
 * @param	rel_y		The relative y position of the widget: 0.0 corresponds to the top
 *				side of the master, 1.0 to the bottom side (defaults to 0.0).
 * @param	rel_width	The relative width of the widget: 0.0 corresponds to a width of 0,
 *				1.0 to the width of the master (defaults to 0.0).
 * @param	rel_height	The relative height of the widget: 0.0 corresponds to a height of 0,
 *				1.0 to the height of the master (defaults to 0.0).
 */
void ei_place(struct ei_widget_t *widget, ei_anchor_t *anchor, int *x, int *y,
	      int *width, int *height, float *rel_x, float *rel_y,
	      float *rel_width, float *rel_height)
{
	if (widget->placer_params == NULL)
		widget->placer_params = malloc(sizeof(ei_placer_params_t));

	widget->placer_params->anchor = anchor;
	widget->placer_params->anchor_data =
		anchor != NULL ? *anchor : ei_anc_northwest;

	widget->placer_params->x = x;
	widget->placer_params->x_data = x != NULL ?
						*x :
						widget->placer_params->x ?
						widget->placer_params->x_data :
						0;
	widget->placer_params->y = y;
	widget->placer_params->y_data = y != NULL ?
						*y :
						widget->placer_params->y ?
						widget->placer_params->y_data :
						0;

	if (rel_width != NULL && rel_width != 0) {
		widget->placer_params->w = width;
	} else {
		widget->placer_params->w = &widget->requested_size.width;
	}

	widget->placer_params->w_data =
		width != NULL ?
			*width :
			(rel_width == NULL) ? widget->requested_size.width : 0;

	if (rel_height != NULL && rel_height != 0) {
		widget->placer_params->h = height;
	} else {
		widget->placer_params->h = &widget->requested_size.height;
	}

	widget->placer_params->h_data = height != NULL ?
						*height :
						(rel_height == NULL) ?
						widget->requested_size.height :
						0;

	widget->placer_params->rx = rel_x;
	widget->placer_params->rx_data =
		rel_x != NULL ? *rel_x :
				widget->placer_params->rx ?
				widget->placer_params->rx_data :
				0.0;
	widget->placer_params->ry = rel_y;
	widget->placer_params->ry_data =
		rel_y != NULL ? *rel_y :
				widget->placer_params->ry ?
				widget->placer_params->ry_data :
				0.0;
	widget->placer_params->rw = rel_width;
	widget->placer_params->rw_data =
		rel_width != NULL ? *rel_width :
				    widget->placer_params->rw ?
				    widget->placer_params->rw_data :
				    0.0;
	widget->placer_params->rh = rel_height;
	widget->placer_params->rh_data =
		rel_height != NULL ? *rel_height :
				     widget->placer_params->rh ?
				     widget->placer_params->rh_data :
				     0.0;

	ei_placer_run(widget);
}

/**
 * \brief	Tells the placer to recompute the geometry of a widget.
 *		The widget must have been previsouly placed by a call to \ref ei_place.
 *		Geometry re-computation is necessary for example when the text label of
 *		a widget has changed, and thus the widget "natural" size has changed.
 *
 * @param	widget		The widget which geometry must be re-computed.
 */
void ei_placer_run(struct ei_widget_t *widget)
{
	if (widget->placer_params == NULL)
		return;

	ei_rect_t parent_params = { { 0, 0 },
				    { widget->placer_params->w_data,
				      widget->placer_params->h_data } };
	if (widget->parent != NULL) {
		parent_params = widget->parent->screen_location;
	}

	int true_x =
		parent_params.top_left.x + widget->placer_params->x_data +
		(widget->placer_params->rx_data * parent_params.size.width);

	int true_y =
		parent_params.top_left.y + widget->placer_params->y_data +
		(widget->placer_params->ry_data * parent_params.size.height);

	int true_w = widget->placer_params->rw == NULL ?
			     widget->placer_params->w_data :
			     widget->placer_params->rw_data *
				     parent_params.size.width;

	if (true_w == 0) {
		true_w = widget->requested_size.width;
	}

	int true_h = widget->placer_params->rh == NULL ?
			     widget->placer_params->h_data :
			     widget->placer_params->rh_data *
				     parent_params.size.height;

	if (true_h == 0) {
		true_h = widget->requested_size.height;
	}

	ei_rect_t rect = { { true_x, true_y }, { true_w, true_h } };

	ei_point_t top_left =
		apply_anchor(widget->placer_params->anchor_data, rect);

	if (widget->wclass->geomnotifyfunc != NULL) {
		widget->wclass->geomnotifyfunc(
			widget, ei_rect(top_left, ei_size(true_w, true_h)));
	}
	widget->content_rect->top_left =
		top_left ;
	widget->content_rect->size.width = true_w;
	widget->content_rect->size.height = true_h;
}

/**
 * \brief	Tells the placer to remove a widget from the screen and forget about it.
 *		Note: the widget is not destroyed and still exists in memory.
 *
 * @param	widget		The widget to remove from screen.
 */
void ei_placer_forget(struct ei_widget_t *widget)
{
	if (widget != NULL && widget->placer_params != NULL) {
		widget->content_rect->size.width = 0;
		widget->content_rect->size.height = 0;
		widget->content_rect->top_left.x = 0;
		widget->content_rect->top_left.y = 0;
		free(widget->placer_params);
		widget->placer_params = NULL;
	}
}
