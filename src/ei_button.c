/**
 *  @file	ei_button.c
 *  @brief	Definition and registration of button class.
 *
 */

#include <stdlib.h>
#include <stdio.h>

#include "hw_interface.h"
#include "ei_draw.h"
#include "ei_widget.h"
#include "ei_types.h"
#include "ei_button.h"
#include "ei_frame.h"
#include "ei_application.h"
#include "ei_utils.h"
#include "button_utils.h"
#include "widget_utils.h"
#include "macro.h"
#include "ei_event.h"

extern ei_surface_t pick_surface;
ei_bool_t clicked = EI_FALSE;

/**
 * \brief	A structure that stores information about the button class.
 */
ei_widgetclass_t button_class = { "button",
				  ei_button_allocfunc,
				  ei_button_releasefunc,
				  ei_button_drawfunc,
				  ei_button_setdefaultsfunc,
				  ei_button_geomnotifyfunc,
				  ei_button_handlefunc,
				  NULL };

/**
 * \brief	A function that allocates a block of memory that is big enough to store the
 *		attributes of a widget of a class. After allocation, the function *must*
 *		initialize the memory to 0.
 *
 * @return		A block of memory with all bytes set to 0.
 */
void *ei_button_allocfunc()
{
	ei_button_widget_t *pointer_ei_button_widget =
		calloc(1, sizeof(ei_button_widget_t));
	return pointer_ei_button_widget;
}

/**
 * \brief	A function that releases the memory used by a widget before it is destroyed.
 *		The \ref ei_widget_t structure itself, passed as parameter, must *not* by freed by
 *		these functions. Can be set to NULL in \ref ei_widgetclass_t if no memory is used by
 *		a class of widget.
 *
 * @param	widget		The widget which resources are to be freed.
 */
void ei_button_releasefunc(struct ei_widget_t *widget)
{
	if (widget == NULL)
		return;
	ei_frame_releasefunc(widget);
}

/**
 * \brief	A function that draws widgets of a class.
 *
 * @param	widget		A pointer to the widget instance to draw.
 * @param	surface		Where to draw the widget. The actual location of the widget in the
 *				surface is stored in its "screen_location" field.
 * @param	pick_surface	Where to draw the pick version of the widget.
 * @param	clipper		If not NULL, the drawing is restricted within this rectangle
 *				(expressed in the surface reference frame).
 */
void ei_button_drawfunc(ei_widget_t *widget, ei_surface_t surface,
			ei_surface_t pick_surface, ei_rect_t *clipper)
{
	if (widget->screen_location.top_left.x >
		    clipper->top_left.x + clipper->size.width ||
	    widget->screen_location.top_left.y >
		    clipper->top_left.y + clipper->size.height ||
	    widget->screen_location.top_left.x +
			    widget->screen_location.size.width <
		    clipper->top_left.x ||
	    widget->screen_location.top_left.y +
			    widget->screen_location.size.height <
		    clipper->top_left.y) {
		return;
	}

	ei_button_widget_t *button = (ei_button_widget_t *)widget;



	if (button->corner_radius == 0) {
		ei_frame_drawfunc(widget, surface, pick_surface, clipper);
	} else {

		hw_surface_lock(pick_surface);

		ei_linked_point_t frame;

		pick_button(pick_surface, *(widget->pick_color), &frame,
			widget->screen_location, button->corner_radius, clipper);

		hw_surface_unlock(pick_surface);

		hw_surface_lock(surface);

		ei_linked_point_t in;
		ei_linked_point_t down;
		ei_linked_point_t up;

		ei_color_t light = color_plus(button->bg_color, 0.70);
		ei_color_t dark = color_plus(button->bg_color, 1.80);

		if (button->relief == ei_relief_raised) {
			draw_button(surface, button->bg_color, dark, light, &in, &down,
				&up, widget->screen_location, button->corner_radius,
				button->border_width, clipper);

		} else if (button->relief == ei_relief_sunken) {
			draw_button(surface, button->bg_color, light, dark, &in, &down,
				&up, widget->screen_location, button->corner_radius,
				button->border_width, clipper);
		}

		if (button->img) {
			// size de l'image
			ei_size_t img_size = hw_surface_get_size(button->img);

			//rectangle parent = bouton;
			int crop_border = (int)(0.3 * button->corner_radius);

			ei_size_t inside_border =
				ei_size(widget->screen_location.size.width -
						2 * button->border_width -
						2 * crop_border,
					widget->screen_location.size.height -
						2 * button->border_width -
						2 * crop_border);
			ei_point_t top_left_inside = ei_point(
				button->border_width + crop_border +
					widget->screen_location.top_left.x,
				button->border_width + crop_border +
					widget->screen_location.top_left.y);

			ei_rect_t inside_rect =
				ei_rect(top_left_inside, inside_border);

			// la vraie position de l'image selon l'ancre
			ei_point_t real_top_left;
			if (img_size.width < inside_border.width &&
			    img_size.height < inside_border.height) {
				real_top_left =
					position_from_anchor(button->img_anchor,
							     inside_rect,
							     img_size, 0);
			} else {
				real_top_left = top_left_inside;
			}
			ei_rect_t dst_rect =
				ei_rect(real_top_left, inside_border);

			ei_copy_surface(surface, &dst_rect, button->img,
					button->img_rect, EI_TRUE);
		}

		if (button->text) {
			// size du texte
			ei_size_t text_size;
			hw_text_compute_size(button->text, button->text_font,
					     &(text_size.width),
					     &(text_size.height));

			//rectangle parent = bouton;
			ei_size_t inside_border =
				ei_size(widget->screen_location.size.width -
						2 * button->border_width,
					widget->screen_location.size.height -
						2 * button->border_width);
			ei_point_t top_left_inside = ei_point(
				button->border_width +
					widget->screen_location.top_left.x,
				button->border_width +
					widget->screen_location.top_left.y);

			ei_rect_t inside_rect =
				ei_rect(top_left_inside, inside_border);

			// la vraie position du texte selon l'ancre
			if (text_size.width >= inside_border.width &&
			    text_size.height >= inside_border.height) {
				button->text_anchor = ei_anc_northwest;
			}
			ei_point_t real_top_left =
				position_from_anchor(button->text_anchor,
						     inside_rect, text_size,
						     button->corner_radius);

			ei_rect_t text_rect = ei_rect(real_top_left, text_size);

			ei_rect_t true_clipper_text = clip_clipper(text_rect, clipper);

			// On dessine le texte par appel à ei_draw_text
			ei_draw_text(surface, &real_top_left, button->text,
				     button->text_font, &(button->text_color),
				     &true_clipper_text);
		}

		hw_surface_unlock(surface);
	}
}

/**
 * \brief	A function that sets the default values for a class.
 *
 * @param	widget		A pointer to the widget instance to intialize.
 */
void ei_button_setdefaultsfunc(ei_widget_t *widget)
{
	ei_button_widget_t *button = (ei_button_widget_t *)widget;
	button->bg_color = ei_default_background_color;
	button->relief = ei_relief_raised;
	button->border_width = k_default_button_border_width;
	button->text = NULL;
	button->text_font = ei_default_font;
	button->text_color = ei_font_default_color;
	button->text_anchor = ei_anc_center;
	button->img = NULL;
	button->img_rect = NULL;
	button->img_anchor = ei_anc_center;
	button->corner_radius = k_default_button_corner_radius;
	button->callback = NULL;
	button->user_param = NULL;
}

/**
 * @brief	A function that is called in response to an event. This function 
 *		is internal to the library. It implements the generic behavior of
 *		a widget (for example a button looks sunken when clicked)
 *
 * @param	widget		The widget for which the event was generated.
 * @param	event		The event containing all its parameters (type, etc.)
 *
 * @return			A boolean telling if the event was consumed by the callback or not.
 *				If TRUE, the library does not try to call other callbacks for this
 *				event. If FALSE, the library will call the next callback registered
 *				for this event, if any.
 *				Note: The callback may execute many operations and still return
 *				FALSE, or return TRUE without having done anything.
 */
ei_bool_t ei_button_handlefunc(ei_widget_t *widget, ei_event_t *event)
{

	ei_button_widget_t *button = (ei_button_widget_t *)widget;

	if(button->callback == NULL)
		return EI_FALSE;

	if (event->type == ei_ev_mouse_buttondown && event->param.mouse.button_number == 1 && !clicked) {
		ei_event_set_active_widget(widget);
		button->relief = inverse_relief(button->relief);
		ei_app_invalidate_rect(&(widget->screen_location));
		clicked = EI_TRUE;
		return EI_TRUE;
	}

	if(ei_event_get_active_widget() != widget) {
		return EI_FALSE;
	}

	if(ei_widget_pick(&(event->param.mouse.where)) != widget && event->type == ei_ev_mouse_buttonup) {
		ei_event_set_active_widget(NULL);

		clicked = EI_FALSE;
		button->relief = inverse_relief(button->relief);
		ei_app_invalidate_rect(&(widget->screen_location));
		return EI_FALSE;
	}

	if (event->type == ei_ev_mouse_buttonup && event->param.mouse.button_number == 1 && clicked) {
		ei_event_set_active_widget(NULL);

		button->callback(widget, event, button->user_param);
		button->relief = inverse_relief(button->relief);
		ei_app_invalidate_rect(&(widget->screen_location));
		clicked = EI_FALSE;
		return EI_TRUE;
	}
	return EI_FALSE;
}

/**
 * \brief 	A function that is called to notify the widget that its geometry has been modified
 *		by its geometry manager. Can set to NULL in \ref ei_widgetclass_t.
 *
 * @param	widget		The widget instance to notify of a geometry change.
 * @param	rect		The new rectangular screen location of the widget
 *				(i.e. = widget->screen_location).
 */
void ei_button_geomnotifyfunc(ei_widget_t *widget, ei_rect_t rect)
{
	ei_rect_t new_rect = ei_rect(
		ei_point(MIN(widget->screen_location.top_left.x, rect.top_left.x),
		  MIN(widget->screen_location.top_left.y, rect.top_left.y)),
		  ei_size(MAX(widget->screen_location.top_left.x + widget->screen_location.size.width,
			rect.top_left.x + rect.size.width),
		  MAX(widget->screen_location.top_left.y + widget->screen_location.size.height,
			rect.top_left.y + rect.size.height)));
	ei_app_invalidate_rect(&new_rect);
	widget->screen_location = rect;
}
