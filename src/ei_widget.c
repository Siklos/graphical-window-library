/**
 * @file	ei_widget.c
 *
 * @brief 	API for widgets management: creation, configuration, hierarchy, redisplay.
 * 
 */
#include <stdlib.h>
#include <string.h>

#include "ei_draw.h"
#include "ei_widgetclass.h"
#include "ei_placer.h"
#include "ei_frame.h"
#include "ei_button.h"
#include "ei_toplevel.h"

#include "draw_utils.h"
#include "widget_utils.h"
#include "macro.h"
#include "ei_application.h"

extern ei_surface_t pick_surface;

uint32_t pick_id = UINT32_MAX;

/**
 * \brief	Decrement the counter pick_id
 * 
 * @return	uint32_t	int	Return the decremented counter pick_ed
 */
uint32_t get_next_pick_id() {
	uint32_t temp = pick_id;
	--pick_id;
	return (temp | 0xff000000);
}

/**
 * \brief
 * 
 * @param	widget
 * @param	new_child
 */
void widget_add_child(ei_widget_t *widget, ei_widget_t *new_child) {
	if(widget == NULL)
		return;
	if (widget->children_head == NULL) {
		widget->children_head = new_child;
		widget->children_tail = new_child;
		new_child->next_sibling = NULL;
		return;
	}
	widget->children_tail->next_sibling = new_child;
	widget->children_tail = new_child;
}
/**
 * @brief	Creates a new instance of a widget of some particular class, as a descendant of
 *		an existing widget.
 *
 *		The widget is not displayed on screen until it is managed by a geometry manager.
 *		The widget should be released by calling \ref ei_widget_destroy when no more needed.
 *
 * @param	class_name	The name of the class of the widget that is to be created.
 * @param	parent 		A pointer to the parent widget. Can not be NULL.
 *
 * @return			The newly created widget, or NULL if there was an error.
 */
ei_widget_t *ei_widget_create(ei_widgetclass_name_t class_name,
			      ei_widget_t *parent)
{
	ei_widgetclass_t * widget_class = ei_widgetclass_from_name(class_name);
	ei_widget_t *widget = (ei_widget_t *) widget_class->allocfunc();
	widget->wclass = widget_class;
	widget->wclass->setdefaultsfunc(widget);
	widget->content_rect = &(widget->screen_location);
	widget->parent = parent;

	widget->pick_id = get_next_pick_id();
	widget->pick_color = (ei_color_t *) &widget->pick_id;

	widget_add_child(parent, widget);
	widget->children_head = NULL;
	widget->children_tail = NULL;
	return widget;
}

/**
 * @brief	Destroys a widget. Removes it from screen if it is managed by a geometry manager.
 *		Destroys all its descendants.
 *
 * @param	widget		The widget that is to be destroyed.
 */
void ei_widget_destroy(ei_widget_t *widget)
{
	if(widget == NULL)
		return;

	if (widget->parent != NULL) {
		if (widget->parent->children_head == widget) {
			widget->parent->children_head = widget->next_sibling;
		} else if (widget->parent->children_tail == widget) {
			ei_widget_t * prec = widget->parent->children_head;
			while (prec->next_sibling != widget) {
				prec = prec->next_sibling;
			}
			prec->next_sibling = NULL;
			widget->parent->children_tail = prec;
		} else {
			ei_widget_t * prec = widget->parent->children_head;
			while (prec->next_sibling != widget) {
				prec = prec->next_sibling;
			}
			prec->next_sibling = widget->next_sibling;
		}

	}

	if (widget->children_head != NULL)
	{
		ei_widget_t *next = widget->children_head;
		ei_widget_t *prev = NULL;
		while (next != NULL) {
			prev = next;
			next = next->next_sibling;
			ei_widget_destroy(prev);
		}
	}
	widget->wclass->geomnotifyfunc(widget, widget->screen_location);
	widget->wclass->releasefunc(widget);
	ei_placer_forget(widget);
	free(widget);
}

/*
 * @brief	find a widget with it's color
 *
 * @param	widget		current widget
 * @param	widget_color	color of the widget
 *
 * @return			Return the widget of a certain color, NULL if there is none
 */
ei_widget_t *find_widget(ei_widget_t* widget, uint32_t widget_id)
{

	if(widget == NULL)
		return NULL;
	if(widget->pick_id == widget_id)
		return widget;
	if(widget->next_sibling != NULL)
		return find_widget(widget->next_sibling, widget_id);
	return find_widget(widget->children_head, widget_id);
}

/**
 * @brief	Returns the widget that is at a given location on screen.
 *
 * @param	where		The location on screen, expressed in the root window coordinates.
 *
 * @return			The top-most widget at this location, or NULL if there is no widget
 *				at this location (except for the root widget).
 */
ei_widget_t *ei_widget_pick(ei_point_t *where)
{
	ei_color_t color = get_color(pick_surface, where->x, where->y);
	uint32_t pick_id = *((uint32_t*) &color);
	return find_widget(ei_app_root_widget(), pick_id);
}

/**
 * @brief	Configures the attributes of widgets of the class "frame".
 *
 *		Parameters obey the "default" protocol: if a parameter is "NULL" and it has never
 *		been defined before, then a default value should be used (default values are
 *		specified for each parameter). If the parameter is "NULL" but was defined on a
 *		previous call, then its value must not be changed.
 *
 * @param	widget		The widget to configure.
 * @param	requested_size	The size requested for this widget, including borders.
 *				The geometry manager may override this size due to other constraints.
 *				Defaults to the "natural size" of the widget, ie. big enough to
 *				display the border and the text or the image. This may be (0, 0)
 *				if the widget has border_width=0, and no text and no image.
 * @param	color		The color of the background of the widget. Defaults to
 *				\ref ei_default_background_color.
 * @param	border_width	The width in pixel of the border decoration of the widget. The final
 *				appearance depends on the "relief" parameter. Defaults to 0.
 * @param	relief		Appearance of the border of the widget. Defaults to
 *				\ref ei_relief_none.
 * @param	text		The text to display in the widget, or NULL. Only one of the
 *				parameter "text" and "img" should be used (i.e. non-NULL). Defaults
 *				to NULL.
 * @param	text_font	The font used to display the text. Defaults to \ref ei_default_font.
 * @param	text_color	The color used to display the text. Defaults to
 *				\ref ei_font_default_color.
 * @param	text_anchor	The anchor of the text, i.e. where it is placed whithin the widget
 *				when the size of the widget is bigger than the size of the text.
 *				Defaults to \ref ei_anc_center.
 * @param	img		The image to display in the widget, or NULL. Any surface can be
 *				used, but usually a surface returned by \ref hw_image_load. Only one
 *				of the parameter "text" and "img" should be used (i.e. non-NULL).
 				Defaults to NULL.
 * @param	img_rect	If not NULL, this rectangle defines a subpart of "img" to use as the
 *				image displayed in the widget. Defaults to NULL.
 * @param	img_anchor	The anchor of the image, i.e. where it is placed whithin the widget
 *				when the size of the widget is bigger than the size of the image.
 *				Defaults to \ref ei_anc_center.
 */
void ei_frame_configure(ei_widget_t *widget, ei_size_t *requested_size,
			const ei_color_t *color, int *border_width,
			ei_relief_t *relief, char **text, ei_font_t *text_font,
			ei_color_t *text_color, ei_anchor_t *text_anchor,
			ei_surface_t *img, ei_rect_t **img_rect,
			ei_anchor_t *img_anchor)
{
	ei_frame_widget_t *frame = (ei_frame_widget_t *)widget;

	if (color != NULL)
		frame->bg_color = *color;

	if (relief != NULL && border_width != NULL &&
	    *relief != ei_relief_none && *border_width > 0) {
		frame->relief = *relief;
	}

	if (text != NULL) {
		if(frame->text != NULL)
			free(frame->text);
		frame->text = malloc(sizeof(char) * (strlen(*text) + 1));
		strcpy(frame->text, *text);
	}

	if (text_font != NULL)
		frame->text_font = *text_font;

	if (text_color != NULL)
		frame->text_color = *text_color;

	if (text_anchor != NULL)
		frame->text_anchor = *text_anchor;

	if (img != NULL)
		frame->img = *img;

	if (img_rect != NULL) {
		if(frame->img_rect != NULL)
			free(img_rect);
		frame->img_rect = malloc(sizeof(ei_rect_t));
		*(frame->img_rect) = **img_rect;
	}

	if (img_anchor != NULL)
		frame->img_anchor = *img_anchor;
	if (border_width != NULL) {
		frame->border_width = *border_width;
	}

	// Defaults to the "natural size" of the widget,
	// ie. big enough to display the border and the text or the image.
	if (requested_size != NULL) {
		widget->requested_size = *requested_size;
	} else {
		if(text != NULL && img == NULL)
			hw_text_compute_size(*text, frame->text_font, &(widget->requested_size.width),
					     &(widget->requested_size.height));
		else if(text == NULL && img != NULL)
			widget->requested_size = hw_surface_get_size(*img);
		else if(text != NULL && img != NULL) {
			ei_size_t text_size;
			hw_text_compute_size(*text, frame->text_font, &text_size.height, &text_size.width);
			ei_size_t img_size = hw_surface_get_size(*img);
			widget->requested_size.height = MAX(text_size.height, img_size.height);
			widget->requested_size.width = MAX(text_size.width, img_size.width);
		}

		if(border_width != NULL) {
			widget->requested_size.height += 2 * *border_width;
			widget->requested_size.width += 2 * *border_width;
		}
	}
	ei_placer_run(widget);
}

/**
 * @brief	Configures the attributes of widgets of the class "button".
 *
 * @param	widget, requested_size, color, border_width, relief,
 *		text, text_font, text_color, text_anchor,
 *		img, img_rect, img_anchor
 *				See the parameter definition of \ref ei_frame_configure. The only
 *				difference is that relief defaults to \ref ei_relief_raised
 *				and border_width defaults to \ref k_default_button_border_width.
 * @param	corner_radius	The radius (in pixels) of the rounded corners of the button.
 *				0 means straight corners. Defaults to \ref k_default_button_corner_radius.
 * @param	callback	The callback function to call when the user clicks on the button.
 *				Defaults to NULL (no callback).
 * @param	user_param	A programmer supplied parameter that will be passed to the callback
 *				when called. Defaults to NULL.
 */
void ei_button_configure(ei_widget_t *widget, ei_size_t *requested_size,
			 const ei_color_t *color, int *border_width,
			 int *corner_radius, ei_relief_t *relief, char **text,
			 ei_font_t *text_font, ei_color_t *text_color,
			 ei_anchor_t *text_anchor, ei_surface_t *img,
			 ei_rect_t **img_rect, ei_anchor_t *img_anchor,
			 ei_callback_t *callback, void **user_param)
{
	ei_button_widget_t *button = (ei_button_widget_t *)widget;
	ei_frame_configure(widget, requested_size,
			   color, border_width,
			   relief, text, text_font,
			   text_color, text_anchor,
			   img, img_rect,
			   img_anchor);

	if(corner_radius != NULL)
		button->corner_radius = *corner_radius;

	if(callback != NULL)
		button->callback = *callback;

	if(user_param != NULL)
		button->user_param = *user_param;
}

/**
 * @brief	Configures the attributes of widgets of the class "toplevel".
 *
 * @param	widget		The widget to configure.
 * @param	requested_size	The content size requested for this widget, this does not include
 *				the decorations	(border, title bar). The geometry manager may
 *				override this size due to other constraints.
 *				Defaults to (320x240).
 * @param	color		The color of the background of the content of the widget. Defaults
 *				to \ref ei_default_background_color.
 * @param	border_width	The width in pixel of the border of the widget. Defaults to 4.
 * @param	title		The string title diplayed in the title bar. Defaults to "Toplevel".
 *				Uses the font \ref ei_default_font.
 * @param	closable	If true, the toplevel is closable by the user, the toplevel must
 *				show a close button in its title bar. Defaults to \ref EI_TRUE.
 * @param	resizable	Defines if the widget can be resized horizontally and/or vertically
 *				by the user. Defaults to \ref ei_axis_both.
 * @param	min_size	For resizable widgets, defines the minimum size. The default minimum
 *				size of a toplevel is (160, 120). If *min_size is NULL, this requires
 *				the toplevel to be configured to its default size.
 */
void ei_toplevel_configure(ei_widget_t *widget, ei_size_t *requested_size,
			   ei_color_t *color, int *border_width, char **title,
			   ei_bool_t *closable, ei_axis_set_t *resizable,
			   ei_size_t **min_size)
{
	ei_toplevel_widget_t *toplevel = (ei_toplevel_widget_t *) widget;

	if (requested_size != NULL)
		widget->requested_size = *requested_size;

	if (color != NULL)
		toplevel->color = *color;

	if (border_width != NULL)
		toplevel->border_width = *border_width;

	if (title != NULL) {
		if(toplevel->title != NULL)
			free(toplevel->title);
		toplevel->title = malloc(sizeof(char) * (strlen(*title) + 1));
		strcpy(toplevel->title, *title);
	}

	if (closable != NULL)
		toplevel->closable = *closable;

	if (resizable != NULL)
		toplevel->resizable = *resizable;

	if (min_size != NULL) {
		if (*min_size == NULL) {
			toplevel->min_size = widget->requested_size;
			return;
		}
		toplevel->min_size = **min_size;
	}
}
