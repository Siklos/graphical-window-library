/**
 *  @file	polygon_utils.c
 *  @brief	List of utils used to draw a polygon
 *
 */

#include <stdlib.h>
#include <stdint.h>

#include "ei_types.h"
#include "hw_interface.h"

#include "polygon_utils.h"
#include "draw_utils.h"

/**
 *  \brief      inializes tc and get is height
 *
 *  @param      first_point     a linked list of points
 *  @param      ymin            minimum height of polygon
 *  @param      ymax            maximum height of polygon
 *  @return                     return an allocated tc list
 */
linked_list_tc_t **initialize_tc(const ei_linked_point_t *first_point,
				 int *ymin, int *ymax)
{
	*ymax = first_point->point.y;
	*ymin = first_point->point.y;

	ei_linked_point_t *current = first_point->next;

	while (current->next != NULL) {
		if (current->point.y < *ymin) {
			*ymin = current->point.y;
		}
		if (current->point.y > *ymax) {
			*ymax = current->point.y;
		}
		current = current->next;
	}

	linked_list_tc_t **tc =
		calloc(sizeof(linked_list_tc_t *) * (*ymax - *ymin),
		       sizeof(linked_list_tc_t *));
	return tc;
}

/**
 *  \brief      fill_tc with value point from a list
 *
 *  @param	tc		The current line
 *  @param      first_point     The first point a linked list of points.
 *  @param      ymin            minimum heihgt of the polygon
 */
void fill_tc(linked_list_tc_t **tc, const ei_linked_point_t *first_point,
	     int ymin)
{
	const ei_linked_point_t *current_point = first_point;
	while (current_point->next != NULL) {
		if (current_point->next->point.y == current_point->point.y) {
			current_point = current_point->next;
			continue;
		}

		int min_y_seg = 0;
		linked_list_tc_t *list = malloc(sizeof(struct linked_list_tc));

		if (current_point->next->point.y > current_point->point.y) {
			min_y_seg = current_point->point.y;
			list->y_max = current_point->next->point.y;
			list->x_y_min = current_point->point.x;
		} else {
			min_y_seg = current_point->next->point.y;
			list->y_max = current_point->point.y;
			list->x_y_min = current_point->next->point.x;
		}

		list->dx =
			(current_point->next->point.x - current_point->point.x);
		list->dy =
			(current_point->next->point.y - current_point->point.y);
		list->e = 0;

		linked_list_tc_t **current_tc_list = &tc[min_y_seg - ymin];
		list->next = *current_tc_list;
		*current_tc_list = list;

		current_point = current_point->next;
	}
}

/**
 *  \brief      frees a linked_list_tc
 *
 *  @param      tca            the list to free
 */
void tca_free(linked_list_tc_t **tca)
{
	linked_list_tc_t *current = *tca;
	while (current->next != NULL) {
		linked_list_tc_t *tmp = current;
		current = current->next;
		free(tmp);
	}
	free(current);
}

/**
 *  \brief      removes element from tca if their eqal to y
 *
 *  @param      tca             the list
 *  @param      y               current heihgt
 */
void tca_remove_equal(linked_list_tc_t **tca, int y)
{
	linked_list_tc_t *current = *tca;
	linked_list_tc_t sent = { 0, 0, 0, 0, 0, *tca };
	linked_list_tc_t *prev = &sent;
	while (current != NULL) {
		if (current->y_max == y) {
			prev->next = current->next;
			free(current);
			current = prev->next;
		} else {
			prev = current;
			current = current->next;
		}
	}
	*tca = sent.next;
}

/**
 *  \brief      fills a scanline of the screen to draw a polygon
 *
 *  @param      surface         the surface to fill
 *  @param      color           fill the scanline with this color
 *  @param      tca             current line
 *  @param      y               height of line to fill
 *  @param      clipper         clipper who bound the zone we can fill
 */
void fill_polygon_scanline(ei_surface_t surface, ei_color_t color,
			   linked_list_tc_t *tca, int y,
			   const ei_rect_t *clipper)
{
	linked_list_tc_t *current = tca;
	int counter = 0;
	while (current != NULL) {
		++counter;
		if ((counter & 1) == 1) {
			for (int x = current->x_y_min;
			     x < current->next->x_y_min; ++x) {
				draw_pixel(surface, x, y, &color, clipper);
			}
		}

		if (current->dx == 0) {
			current = current->next;
			continue;
		}

		int increment = 1;
		int dx = current->dx;
		if (dx < 0) {
			increment = -1;
			dx = -dx;
		}

		int dy = current->dy;
		if (dy < 0) {
			increment = -increment;
			dy = -dy;
		}
		current->e += dx;
		while (2 * current->e >= dy) {
			current->x_y_min += increment;
			current->e -= dy;
		}

		current = current->next;
	}
}

/**
 *  \brief      splits a tc into two tc to merge sort them
 *
 *  @param      src         original list
 *  @param      front       first half of the list
 *  @param      back        last half of the list
 */
void tc_split(linked_list_tc_t *src, linked_list_tc_t **front,
	      linked_list_tc_t **back)
{
	if (src == NULL || src->next == NULL) {
		*front = src;
		*back = NULL;
		return;
	}

	linked_list_tc_t *slow = src;
	linked_list_tc_t *fast = src->next;

	while (fast != NULL) {
		fast = fast->next;
		if (fast != NULL) {
			slow = slow->next;
			fast = fast->next;
		}
	}
	*front = src;
	*back = slow->next;
	slow->next = NULL;
}

/**
 *  \brief      merges the list and sort it
 *
 *  @param      list1        first half of the list
 *  @param      list2        second half of the list
 */
linked_list_tc_t *tc_merge_and_sort(linked_list_tc_t *list1,
				    linked_list_tc_t *list2)
{
	if (list1 == NULL) {
		return list2;
	}
	if (list2 == NULL) {
		return list1;
	}

	linked_list_tc_t *res = NULL;

	if (list1->x_y_min <= list2->x_y_min) {
		res = list1;
		res->next = tc_merge_and_sort(list1->next, list2);
	} else {
		res = list2;
		res->next = tc_merge_and_sort(list1, list2->next);
	}

	return res;
}

/**
 *  \brief      merges sort a list
 *
 *  @param      head        head of the list to sort
 */
void tc_merge_sort(linked_list_tc_t **head)
{
	if (*head == NULL || (*head)->next == NULL) {
		return;
	}
	linked_list_tc_t *list1 = NULL;
	linked_list_tc_t *list2 = NULL;

	tc_split(*head, &list1, &list2);

	tc_merge_sort(&list1);
	tc_merge_sort(&list2);

	*head = tc_merge_and_sort(list1, list2);
}
