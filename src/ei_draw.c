/**
 *  @file	ei_draw.c
 *  @brief	Graphical primitives to draw lines, polygons, text, and operation of drawing
 *		surfaces.
 *
 */

#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <stdio.h>

#include "ei_draw.h"
#include "ei_types.h"
#include "hw_interface.h"
#include "ei_utils.h"

#include "draw_utils.h"
#include "polygon_utils.h"
#include "bresenham_utils.h"
#include "macro.h"

/**
 * \brief			Draws a line with "the Bresenham Algorithm".
 *				Draw_pixel is called for each pixel of the line.
 *
 * @param	surface		where to draw the line
 * @param	point0		First point of the line.
 * @param	point1		Second point of the line.
 * @param	color		Pixel's color.
 * @param	clipper		Can be NULL, in this case, draw on the entire window.
 *				If not NULL, the drawing is restricted within this rectangle.
 */

void draw_line(ei_surface_t surface, ei_point_t point0, ei_point_t point1,
	       const ei_color_t *color, const ei_rect_t *clipper)
{
	// draw_line_bresenham(surface, point0, point1, color, clipper);
	int dx = point1.x - point0.x;
	int dy = point1.y - point0.y;
	if (dx == 0 && dy == 0) {
		draw_pixel(surface, point0.x, point1.y, color, clipper);
		return;
	}
	if (abs(point1.y - point0.y) < abs(point1.x - point0.x)) {
		if (point0.x > point1.x)
			draw_line_bresenham_dx(surface, point1, point0, -dy,
					       -dx, color, clipper);
		else
			draw_line_bresenham_dx(surface, point0, point1, dy, dx,
					       color, clipper);
	} else {
		if (point0.y > point1.y)
			draw_line_bresenham_dy(surface, point1, point0, -dx,
					       -dy, color, clipper);
		else
			draw_line_bresenham_dy(surface, point0, point1, dx, dy,
					       color, clipper);
	}
}

/**
 * \brief	Draws a line made of many line segments.
 *
 * @param	surface 	Where to draw the line. The surface must be *locked* by
 *				\ref hw_surface_lock.
 * @param	first_point 	The head of a linked list of the points of the line. It can be NULL
 *				(i.e. draws nothing), can have a single point, or more.
 *				If the last point is the same as the first point, then this pixel is
 *				drawn only once.
 * @param	color		The color used to draw the line, alpha channel is managed.
 * @param	clipper		If not NULL, the drawing is restricted within this rectangle.
 */

void ei_draw_polyline(ei_surface_t surface,
		      const ei_linked_point_t *first_point,
		      const ei_color_t color, const ei_rect_t *clipper)
{
	if (first_point == NULL) {
		return;
	}
	ei_linked_point_t *second_point = first_point->next;
	while (second_point != NULL) {
		draw_line(surface, first_point->point, second_point->point,
			  &color, clipper);
		first_point = first_point->next;
		second_point = second_point->next;
	}
}

/**
 * \brief	Fills the surface with the specified color.
 *
 * @param	surface		The surface to be filled. The surface must be *locked* by
 *				\ref hw_surface_lock.
 * @param	color		The color used to fill the surface. If NULL, it means that the
 *				caller want it painted black (opaque).
 * @param	clipper		If not NULL, the drawing is restricted within this rectangle.
 */
void ei_fill(ei_surface_t surface, const ei_color_t *color,
	     const ei_rect_t *clipper)
{
	if (color == NULL) {
		ei_color_t default_color = { 0x00, 0x00, 0x00, 0xff };
		color = &default_color;
	}

	if (color->alpha == 0x00) {
		return;
	}

	ei_point_t start_point = ei_point(0, 0);
	ei_size_t buff_size = hw_surface_get_size(surface);
	ei_size_t size = buff_size;

	if (clipper != NULL) {
		start_point = clipper->top_left;
		size = clipper->size;
	}

	int max_width = start_point.x + size.width;
	int max_height = start_point.y + size.height;
	uint32_t *pixel_ptr = (uint32_t *)hw_surface_get_buffer(surface);
	for (int y = start_point.y; y < max_height; ++y) {
		for (int x = start_point.x; x < max_width; ++x) {
			if (x >= 0 && y >= 0 && x < buff_size.width && y < buff_size.height)
				pixel_ptr[x + buff_size.width * y] =
					get_map_rgba(surface, x, y, color);
		}
	}
}

/**
 * \brief	Copies a surface, or a subpart, to another one.
 *		The source and destination area of the copy (either the entire surfaces, or
 *		subparts) must have the same size (before clipping). Both the source and destination
 *		surfaces must be *locked* by \ref hw_surface_lock.
 *
 * @param	destination	The surface on which to copy pixels from the source surface.
 * @param	dst_rect	If NULL, the entire destination surface is used. If not NULL,
 *				defines the rectangle on the destination surface where to copy
 *				the pixels.
 * @param	source		The surface from which to copy pixels.
 * @param	src_rect	If NULL, the entire source surface is used. If not NULL, defines the
 *				rectangle on the source surface from which to copy the pixels.
 * @param	alpha		If true, the final pixels are a combination of source and
 *				destination pixels weighted by the source alpha channel. The
 *				transparency of the final pixels is set	to opaque.
 *				If false, the final pixels are an exact copy of the source pixels,
 				including the alpha channel.
 *
 * @return			Returns 0 on success, 1 on failure (different ROI size).
 */
int ei_copy_surface(ei_surface_t destination, const ei_rect_t *dst_rect,
		    const ei_surface_t source, const ei_rect_t *src_rect,
		    const ei_bool_t alpha)
{
	uint32_t *pixel_ptr_src = (uint32_t *)hw_surface_get_buffer(source);
	ei_rect_t source_surface_rect = hw_surface_get_rect(source);

	ei_size_t size_src = hw_surface_get_size(source);
	ei_size_t size_dst = hw_surface_get_size(destination);

	ei_size_t max_size = size_src;
	ei_point_t top_left = source_surface_rect.top_left;

	if (src_rect != NULL) {
		top_left.y = src_rect->top_left.y;
		top_left.x = src_rect->top_left.x;
		max_size.height = top_left.y + MIN(max_size.height, src_rect->size.height);
		max_size.width = top_left.x + MIN(max_size.width, src_rect->size.width);
	}

	if (size_dst.height < max_size.height &&
	    size_dst.width < max_size.width) {
		return 1;
	}
	ei_point_t pt_dst = dst_rect != NULL ? dst_rect->top_left : ei_point_zero();
	for (int y_src = top_left.y; y_src <  max_size.height; y_src++) {
		for (int x_src = top_left.x; x_src < max_size.width; x_src++) {
			int y_dst = y_src - top_left.y + pt_dst.y;
			int x_dst = x_src - top_left.x + pt_dst.x;

			ei_color_t color = buffer_to_color(
				source,
				pixel_ptr_src[x_src + y_src * size_src.width]);
			if (!alpha) {
				color.alpha = 0xff;
			}
			draw_pixel(destination, x_dst, y_dst, &color, dst_rect);
		}
	}
	return 0;
}

/**
 * \brief	Draws text by calling \ref hw_text_create_surface.
 *
 * @param	surface 	Where to draw the text. The surface must be *locked* by
 *				\ref hw_surface_lock.
 * @param	where		Coordinates, in the surface, where to anchor the top-left corner of
 *				the rendered text.
 * @param	text		The string of the text. Can't be NULL.
 * @param	font		The font used to render the text. If NULL, the \ref ei_default_font
 *				is used.
 * @param	color		The text color. Can't be NULL. The alpha parameter is not used.
 * @param	clipper		If not NULL, the drawing is restricted within this rectangle.
 */
void ei_draw_text(ei_surface_t surface, const ei_point_t *where,
		  const char *text, const ei_font_t font,
		  const ei_color_t *color, const ei_rect_t *clipper)
{
	ei_surface_t source = hw_text_create_surface(text, font, color);
	hw_surface_lock(source);

	ei_rect_t dst_rect;
	ei_rect_t text_clipper;
	ei_size_t text_size = hw_surface_get_size(source);

	if (!is_in_clipper(where->x, where->y, clipper)) {

		ei_point_t top_left_inside;
		dst_rect = *clipper;
		if (where->x < clipper->top_left.x) {
			top_left_inside.x = clipper->top_left.x - where->x;
		} else {
			top_left_inside.x = 0;
		}
		if (where->y < clipper->top_left.y) {
			top_left_inside.y = clipper->top_left.y - where->y;
		} else {
			top_left_inside.y = 0;
		}
		text_clipper.top_left = top_left_inside;
		text_clipper.size.width = text_size.width - text_clipper.top_left.x;
		text_clipper.size.height = text_size.height - text_clipper.top_left.y;

	} else {

		text_clipper.top_left = ei_point(0, 0);
		dst_rect.top_left = *where;

		if (!clipper) {
			dst_rect.size = text_size;

		} else {
			dst_rect.size.width = MIN(text_size.width,
				clipper->top_left.x + clipper->size.width - where->x);
			dst_rect.size.height = MIN(text_size.height,
				clipper->top_left.y + clipper->size.height - where->y);
		}

		text_clipper.size = dst_rect.size;
	}


	int err = ei_copy_surface(surface, &dst_rect, source, &text_clipper, EI_TRUE);
	if (err) {
		fprintf(stderr, "Font bigger than destination");
	}

	hw_surface_unlock(source);
	hw_surface_free(source);
}


/**
 * \brief	Draws a filled polygon.
 *
 * @param	surface 	Where to draw the polygon. The surface must be *locked* by
 *				\ref hw_surface_lock.
 * @param	first_point 	The head of a linked list of the points of the line. It is either
 *				NULL (i.e. draws nothing), or has more than 2 points.
 * @param	color		The color used to draw the polygon, alpha channel is managed.
 * @param	clipper		If not NULL, the drawing is restricted within this rectangle.
 */
void ei_draw_polygon(ei_surface_t surface, const ei_linked_point_t *first_point,
		     const ei_color_t color, const ei_rect_t *clipper)
{
	if (first_point == NULL) {
		return;
	}

	int ymin = 0;
	int ymax = 0;
	linked_list_tc_t **tc = initialize_tc(first_point, &ymin, &ymax);

	fill_tc(tc, first_point, ymin);

	linked_list_tc_t *tca = NULL;
	for (int y = ymin; y < ymax; ++y) {
		linked_list_tc_t *end_tc = tc[y - ymin];
		if (end_tc != NULL) {
			while (end_tc->next != NULL) {
				end_tc = end_tc->next;
			}
			end_tc->next = tca;
			tca = tc[y - ymin];
			tca_remove_equal(&tca, y);
			tc_merge_sort(&tca);
		}
		fill_polygon_scanline(surface, color, tca, y, clipper);
	}

	tca_free(&tca);
	free(tc);
}
