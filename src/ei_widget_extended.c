/**
 * @file	ei_widget_extended.c
 * @brief	Configure extensions of a widget
 */

#include "ei_widget_extended.h"

/**
 * \brief			Configures post widget destroy callback function to be called after a widget is destroyed.
 *
 * @param	widget		The widget to configure.
 *
 * @param	callback	The callback function that will be called after the destruction of the widget.
 *
 * @param	user_param	The parameters used in the callback function as its arguments.
 *
 */
void ei_widget_set_destroy_cb	(ei_widget_t*		widget,
				 ei_callback_t		callback,
				 void*			user_param)
{
	ei_widget_extended_t *widget_extended = (ei_widget_extended_t *) widget;
	widget_extended->destroy_cb = callback;
	widget_extended->destroy_cb_param = user_param;
}
