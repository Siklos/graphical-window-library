# test effectué

Nous avons effectué les tests suivant :

## frame

Nous créons deux frames : la seconde a pour parent la première ; la taille de la seconde est déterminée en fonction de la taille de l'image.

## lines

On dessine plusieurs forme avec des listes de point pour tester Bresenham notamment :

- une ligne simple
- un octogone
- un carré
- un point
- une flèche tronquée

## hello world

On crée deux fenêtres dont une avec un bouton ancré au sud-est.

## double toplevel

Deux toplevel l'un dans l'autre tester le clipping d'un toplevel dans un autre.

## puzzle

Test d'un puzzle à partir d'une image initiale.

## two048

Jeu du 2048

## text

Affichage d'un message à l'écran et clipping de ce dernier.

## polygon

Affichage de plusieurs polygones (dont certains superposés) pour tester indépendamment, nous commentons les lignes supplémentaires.

## lot_frame

Tester le positionnement et l'ancrage de plusieurs frames et test du parcours des fils pour le dessin.

## minimal

Test de l'initialisation d'une fenêtre remplie en blanc.

## draw_button

Dessine la forme d'un bouton.

## double_toplevel

Gestion d'un widget toplevel imbriqué dans un autre toplevel

## button

Un bouton avec une image à l'intérieur trop grande donc clippée.

## toplevel

Crée un toplevel.
