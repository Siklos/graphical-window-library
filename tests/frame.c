#include <stdio.h>
#include <stdlib.h>

#include "ei_application.h"
#include "ei_event.h"
#include "hw_interface.h"
#include "ei_widget.h"

ei_bool_t process_key(ei_event_t *event)
{
	if (event->type == ei_ev_keydown)
		if (event->param.key.key_sym == SDLK_ESCAPE) {
			ei_app_quit_request();
			return EI_TRUE;
		}

	return EI_FALSE;
}

/*
 * ei_main --
 *
 *	Main function of the application.
 */
int ei_main(int argc, char** argv)
{
	ei_size_t	screen_size		= {600, 600};
	ei_color_t	root_bgcol		= {0x52, 0x7f, 0xb4, 0xff};

	ei_widget_t*	frame, *subframe;
	ei_size_t	frame_size		= {300,200};
	int		    subframe_x		= 100;
	int		    subframe_y		= 100;
	ei_color_t	frame_color		= {0x88, 0x88, 0x88, 0xff};
	ei_color_t	subframe_color		= {0xff, 0x00, 0x00, 0x10};
	ei_relief_t	frame_relief		= ei_relief_raised;
	int		frame_border_width	= 6;

	char*		text			= "bonjour";

	/* Create the application and change the color of the background. */
	ei_app_create(&screen_size, EI_FALSE);
	ei_frame_configure(ei_app_root_widget(), NULL, &root_bgcol, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

	/* Create, configure and place the frame on screen. */
	int coord_test1 = 30;
	frame = ei_widget_create("frame", ei_app_root_widget());

	ei_frame_configure(frame, &frame_size, &frame_color,
			   &frame_border_width, &frame_relief, NULL, NULL, NULL,
			   NULL, NULL, NULL, NULL);
	ei_place(frame, NULL, &coord_test1, &coord_test1, &frame_size.width,
		 &frame_size.height, NULL, NULL, NULL, NULL);


	ei_surface_t img = hw_image_load("misc/img.png", ei_app_root_surface());
	subframe = ei_widget_create("frame", frame);
	ei_frame_configure(subframe, NULL, &subframe_color,
			   &frame_border_width, &frame_relief, &text, NULL,
			   img, NULL, &img, NULL, NULL);

	ei_place(subframe, NULL, &subframe_x, &subframe_y, NULL, NULL, NULL, NULL, NULL,
		 NULL);

	ei_event_set_default_handle_func(process_key);

	/* Run the application's main loop. */
	ei_app_run();

	/* We just exited from the main loop. Terminate the application (cleanup). */
	ei_app_free();

	return (EXIT_SUCCESS);
}
