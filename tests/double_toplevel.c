#include <stdio.h>
#include <stdlib.h>

#include "ei_application.h"
#include "ei_event.h"
#include "hw_interface.h"
#include "ei_widget.h"

/*
 * button_press --
 *
 *	Callback called when a user clicks on the button.
 */
void button_press(ei_widget_t *widget, ei_event_t *event, void *user_param)
{
	printf("Click !\n");
}

/*
 * process_key --
 *
 *	Callback called when any key is pressed by the user.
 *	Simply looks for the "Escape" key to request the application to quit.
 */
ei_bool_t process_key(ei_event_t *event)
{
	if (event->type == ei_ev_keydown)
		if (event->param.key.key_sym == SDLK_ESCAPE) {
			ei_app_quit_request();
			return EI_TRUE;
		}

	return EI_FALSE;
}

/*
 * ei_main --
 *
 *	Main function of the application.
 */
int ei_main(int argc, char **argv)
{
	ei_size_t screen_size = { 800, 600 };
	ei_color_t root_bgcol = { 0x52, 0x7f, 0xb4, 0xff };

	ei_widget_t *window1;
	ei_size_t window_size_1 = { 600, 400 };
	char *window_title_1 = "main toplevel";
	ei_color_t window_color_1 = { 0xA0, 0xA0, 0xA0, 0xff };
	int window_border_width_1 = 4;
	ei_bool_t window_closable = EI_TRUE;
	ei_axis_set_t window_resizable = ei_axis_both;
	ei_point_t window_position_1 = { 50, 50 };

	ei_widget_t *window2;
	ei_size_t window_size_2 = { 300, 200 };
	char *window_title_2 = "subordinate toplevel";
	ei_color_t window_color_2 = { 0xB0, 0xB0, 0xB0, 0xff };
	int window_border_width_2 = 2;
	ei_point_t window_position_2 = { 100, 100 };



	/* Create the application and change the color of the background. */
	ei_app_create(&screen_size, EI_FALSE);
	ei_frame_configure(ei_app_root_widget(), NULL, &root_bgcol, NULL, NULL,
			   NULL, NULL, NULL, NULL, NULL, NULL, NULL);
	ei_event_set_default_handle_func(process_key);

	/* Create, configure and place the main toplevel window on screen. */
	window1 = ei_widget_create("toplevel", ei_app_root_widget());
	ei_toplevel_configure(window1, &window_size_1, &window_color_1,
			      &window_border_width_1, &window_title_1,
			      &window_closable, &window_resizable, NULL);
	ei_place(window1, NULL, &(window_position_1.x), &(window_position_1.y), NULL,
		 NULL, NULL, NULL, NULL, NULL);


	/* Create, configure and place the subordinate toplevel window on screen. */
	window2 = ei_widget_create("toplevel", window1);
	ei_toplevel_configure(window2, &window_size_2, &window_color_2,
			      &window_border_width_2, &window_title_2,
			      &window_closable, &window_resizable, NULL);

	ei_place(window2, NULL, &(window_position_2.x), &(window_position_2.y), NULL,
		 NULL, NULL, NULL, NULL, NULL);

	ei_app_run();

	ei_app_free();

	return (EXIT_SUCCESS);
}
