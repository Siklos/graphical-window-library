#include <stdio.h>
#include <stdlib.h>

#include "ei_event.h"
#include "ei_application.h"
#include "ei_event.h"
#include "hw_interface.h"
#include "ei_widget.h"
#include "ei_types.h"

ei_bool_t process_key(ei_event_t *event)
{
	if (event->type == ei_ev_keydown)
		if (event->param.key.key_sym == SDLK_ESCAPE) {
			ei_app_quit_request();
			return EI_TRUE;
		}

	return EI_FALSE;
}

/*
 * ei_main --
 *
 *	Main function of the application.
 */
int ei_main(int argc, char **argv)
{
	ei_size_t screen_size = { 600, 600 };
	ei_color_t root_bgcol = { 0x52, 0x7f, 0xb4, 0xff };

	ei_widget_t *frame, *subframe;
	ei_size_t frame_size = { 300, 300 };
	ei_color_t frame_color = { 0x88, 0x88, 0x88, 0xff };
	ei_size_t sub_frame_size = { 50, 50 };
	ei_color_t red = { 0xff, 0x00, 0x00, 0xff };
	ei_relief_t frame_relief = ei_relief_raised;
	int frame_border_width = 6;
	ei_anchor_t anc[5] = { ei_anc_southeast, ei_anc_southwest,
			       ei_anc_northwest, ei_anc_northeast, ei_anc_center};

	/* Create the application and change the color of the background. */
	ei_app_create(&screen_size, EI_FALSE);
	ei_frame_configure(ei_app_root_widget(), NULL, &root_bgcol, NULL, NULL,
			   NULL, NULL, NULL, NULL, NULL, NULL, NULL);
	float middle = 0.5;
	/* Cardinal tests */
	for(int i = 0; i < 5; i++) {
		frame = ei_widget_create("frame", ei_app_root_widget());
		ei_frame_configure(frame, &frame_size, &frame_color,
				   &frame_border_width, &frame_relief, NULL,
				   NULL, NULL, NULL, NULL, NULL, NULL);
		ei_place(frame, &anc[i], NULL, NULL, NULL, NULL, &middle,
			 &middle, &middle, &middle);
	}

	int coord_test1 = 30;
	
	frame = ei_widget_create("frame", ei_app_root_widget());
	
	ei_frame_configure(frame, &frame_size, &frame_color,
				&frame_border_width, &frame_relief, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL);
	ei_place(frame, &anc[2], &coord_test1, &coord_test1, &frame_size.width,
		 &frame_size.height, NULL, NULL, NULL, NULL);

	subframe = ei_widget_create("frame", frame);
	ei_frame_configure(subframe, &sub_frame_size, &red, &frame_border_width,
			   &frame_relief, NULL, NULL, NULL, NULL, NULL, NULL,
			   NULL);

	ei_place(subframe, &anc[2], &frame_border_width, &frame_border_width,
		 NULL, NULL, NULL, NULL, NULL, NULL);

	ei_event_set_default_handle_func(process_key);

	/* Run the application's main loop. */
	ei_app_run();

	/* We just exited from the main loop. Terminate the application (cleanup). */
	ei_app_free();

	return (EXIT_SUCCESS);
}
