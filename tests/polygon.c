#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "hw_interface.h"
#include "ei_utils.h"
#include "ei_draw.h"
#include "ei_types.h"

/* test_octogone --
 *
 *	Draws an octogone in the middle of the screen. This is meant to test the
 *	algorithm that draws a polyline in each of the possible octants, that is,
 *	in each quadrant with dx>dy (canonical) and dy>dx (steep).
 */
void test_octogone(ei_surface_t surface, ei_rect_t *clipper)
{
	ei_color_t color = { 0, 255, 0, 255 };
	ei_linked_point_t pts[9];
	int i, xdiff, ydiff;

	/* Initialisation */
	pts[0].point.x = 400;
	pts[0].point.y = 90;

	/* Draw the octogone */
	for (i = 1; i <= 8; i++) {
		/*	Add or remove 70/140 pixels for next point
			The first term of this formula gives the sign + or - of the operation
			The second term is 2 or 1, according to which coordinate grows faster
			The third term is simply the amount of pixels to skip */
		xdiff = pow(-1, (i + 1) / 4) * pow(2, (i / 2) % 2 == 0) * 70;
		ydiff = pow(-1, (i - 1) / 4) * pow(2, (i / 2) % 2) * 70;

		pts[i].point.x = pts[i - 1].point.x + xdiff;
		pts[i].point.y = pts[i - 1].point.y + ydiff;
		pts[i - 1].next = &(pts[i]);
	}

	/* End the linked list */
	pts[i - 1].next = NULL;

	/* Draw the form with polygon */
	ei_draw_polygon(surface, pts, color, clipper);
}

/* test_triangle --
 *
 *	Draws an octogone in the middle of the screen. This is meant to test the
 *	algorithm that draws a polyline in each of the possible octants, that is,
 *	in each quadrant with dx>dy (canonical) and dy>dx (steep).
 */
void test_triangle(ei_surface_t surface, ei_rect_t *clipper)
{
	ei_color_t color = { 123, 123, 0, 255 };
	ei_linked_point_t pts[5];

	/* Initialisation */
	pts[0].point.x = 400;
	pts[0].point.y = 90;
	pts[1].point.x = 500;
	pts[1].point.y = 160;
	pts[2].point.x = 300;
	pts[2].point.y = 160;
	pts[3].point.x = 400;
	pts[3].point.y = 90;

	pts[0].next = &(pts[2]);
	pts[1].next = &(pts[3]);
	pts[2].next = &(pts[1]);

	/* End the linked list */
	pts[3].next = NULL;

	/* Draw the form with polygon */
	ei_draw_polygon(surface, pts, color, clipper);
}

/* test_square --
 *
 *	Draws a square in the middle of the screen. This is meant to test the
 *	algorithm for the special cases of horizontal and vertical lines, where
 *	dx or dy are zero
 */
void test_square(ei_surface_t surface, ei_rect_t *clipper)
{
	ei_color_t color = { 81, 26, 174, 255 };
	ei_linked_point_t pts[5];
	int i, xdiff, ydiff;

	/* Initialisation */
	pts[0].point.x = 300;
	pts[0].point.y = 400;

	/* Draw the square */
	for (i = 1; i <= 4; i++) {
		/*	Add or remove 200 pixels or 0 for next point
			The first term of this formula gives the sign + or - of the operation
			The second term is 0 or 1, according to which coordinate grows
			The third term is simply the side of the square */
		xdiff = pow(-1, i / 2) * (i % 2) * 200;
		ydiff = pow(-1, i / 2) * (i % 2 == 0) * 200;

		pts[i].point.x = pts[i - 1].point.x + xdiff;
		pts[i].point.y = pts[i - 1].point.y + ydiff;
		pts[i - 1].next = &(pts[i]);
	}

	/* End the linked list */
	pts[i - 1].next = NULL;

	/* Draw the form with polygon */
	ei_draw_polygon(surface, pts, color, clipper);
}

/* test_square --
 *
 *	Draws a square in the middle of the screen. This is meant to test the
 *	algorithm for the special cases of horizontal and vertical lines, where
 *	dx or dy are zero
 */
void test_sablier(ei_surface_t surface, ei_rect_t *clipper)
{
	ei_color_t color = { 255, 255, 0, 255 };
	ei_linked_point_t pts[7];

	/* Initialisation */
	pts[0].point.x = 200;
	pts[0].point.y = 200;

	pts[1].point.x = 400;
	pts[1].point.y = 200;

	pts[2].point.x = 350;
	pts[2].point.y = 300;

	pts[3].point.x = 400;
	pts[3].point.y = 400;

	pts[4].point.x = 200;
	pts[4].point.y = 400;

	pts[5].point.x = 250;
	pts[5].point.y = 300;

	pts[6].point.x = 200;
	pts[6].point.y = 200;

	pts[0].next = &pts[1];
	pts[1].next = &pts[2];
	pts[2].next = &pts[3];
	pts[3].next = &pts[4];
	pts[4].next = &pts[5];
	pts[5].next = &pts[6];
	pts[6].next = NULL;

	/* Draw the form with polygon */
	ei_draw_polygon(surface, pts, color, clipper);
}

/* test_arrow --
 *
 *	Draws a square in the middle of the screen. This is meant to test the
 *	algorithm for the special cases of horizontal and vertical lines, where
 *	dx or dy are zero
 */
void test_idiot(ei_surface_t surface, ei_rect_t *clipper)
{
	ei_color_t color = { 0, 0, 255, 255 };
	ei_linked_point_t pts[6];

	/* Initialisation */
	pts[0].point.x = 300;
	pts[0].point.y = 300;
	pts[0].next = &pts[1];

	pts[1].point.x = 500;
	pts[1].point.y = 299;
	pts[1].next = &pts[2];

	pts[2].point.x = 300;
	pts[2].point.y = 300;
	pts[2].next = NULL;

	/* Draw the form with polygon */
	ei_draw_polygon(surface, pts, color, clipper);
}

void test_tr(ei_surface_t surface, ei_rect_t *clipper)
{
	ei_color_t color = { 64, 21, 158, 255 };
	ei_linked_point_t pts[6];

	/* Initialisation */
	pts[0].point.x = 300;
	pts[0].point.y = 100;
	pts[0].next = &pts[1];

	pts[1].point.x = 300;
	pts[1].point.y = 300;
	pts[1].next = &pts[2];

	pts[2].point.x = 100;
	pts[2].point.y = 300;
	pts[2].next = &pts[3];

	pts[3].point.x = 300;
	pts[3].point.y = 100;
	pts[3].next = NULL;

	/* Draw the form with polygon */
	ei_draw_polygon(surface, pts, color, clipper);
}

/* test_arrow --
 *
 *	Draws a square in the middle of the screen. This is meant to test the
 *	algorithm for the special cases of horizontal and vertical lines, where
 *	dx or dy are zero
 */
void test_arrow(ei_surface_t surface, ei_rect_t *clipper)
{
	ei_color_t color = { 56, 174, 154, 255 };
	ei_linked_point_t pts[6];

	/* Initialisation */
	pts[0].point.x = 300;
	pts[0].point.y = 200;
	pts[0].next = &pts[5];

	pts[5].point.x = 500;
	pts[5].point.y = 200;
	pts[5].next = &pts[1];

	pts[1].point.x = 600;
	pts[1].point.y = 400;
	pts[1].next = &pts[2];

	pts[2].point.x = 400;
	pts[2].point.y = 300;
	pts[2].next = &pts[3];

	pts[3].point.x = 200;
	pts[3].point.y = 400;
	pts[3].next = &pts[4];

	pts[4].point.x = 300;
	pts[4].point.y = 200;

	/* End the linked list */
	pts[4].next = NULL;

	/* Draw the form with polygon */
	ei_draw_polygon(surface, pts, color, clipper);
}

/* test_two_figs --
 *
 *	Draws a square in the middle of the screen. This is meant to test the
 *	algorithm for the special cases of horizontal and vertical lines, where
 *	dx or dy are zero
 */
void test_two_figs(ei_surface_t surface, ei_rect_t *clipper)
{
	ei_color_t color2 = { 255, 255, 255, 255 };
	ei_color_t color1 = { 0, 255, 0, 255 };
	ei_linked_point_t pts1[5];
	ei_linked_point_t pts2[4];

	/* Initialisation */
	pts1[0].point.x = 300;
	pts1[0].point.y = 50;
	pts1[0].next = &pts1[1];

	pts1[1].point.x = 300;
	pts1[1].point.y = 450;
	pts1[1].next = &pts1[2];

	pts1[2].point.x = 550;
	pts1[2].point.y = 450;
	pts1[2].next = &pts1[3];

	pts1[3].point.x = 400;
	pts1[3].point.y = 50;
	pts1[3].next = &pts1[4];

	pts1[4].point.x = 300;
	pts1[4].point.y = 50;
	pts1[4].next = NULL;

	/* Initialisation */
	pts2[0].point.x = 400;
	pts2[0].point.y = 50;
	pts2[0].next = &pts2[1];

	pts2[1].point.x = 650;
	pts2[1].point.y = 450;
	pts2[1].next = &pts2[2];

	pts2[2].point.x = 550;
	pts2[2].point.y = 450;
	pts2[2].next = &pts2[3];

	pts2[3].point.x = 400;
	pts2[3].point.y = 50;
	pts2[3].next = NULL;

	/* Draw the form with polygon */

	ei_draw_polygon(surface, pts2, color2, clipper);

	ei_draw_polygon(surface, pts1, color1, clipper);

	// ei_color_t color3 = { 255, 0, 0, 255 };
	// ei_color_t color4 = { 255, 255, 255, 255 };
	// draw_line(surface, pts1[2].point, pts1[3].point, &color3, clipper);
	// draw_line(surface, pts2[0].point, pts2[2].point, &color3, clipper);
}

/*
 * ei_main --
 *
 *	Main function of the application.
 */
int ei_main(int argc, char **argv)
{
	ei_size_t win_size = ei_size(800, 800);
	ei_surface_t main_window = NULL;
	ei_color_t white = { 0xff, 0xff, 0xff, 0xff };
	ei_rect_t *clipper_ptr = NULL;
	// ei_rect_t		clipper		= ei_rect(ei_point(200, 150), ei_size(400, 300));
	// clipper_ptr		= &clipper;

	hw_init();

	main_window = hw_create_window(&win_size, EI_FALSE);

	/* Lock the drawing surface, paint it white. */
	hw_surface_lock(main_window);
	ei_fill(main_window, &white, clipper_ptr);

	/* Draw polygon. */
	test_octogone(main_window, clipper_ptr);
	// test_triangle(main_window, clipper_ptr);
	// test_square(main_window, clipper_ptr);
	test_arrow(main_window, clipper_ptr);
	// test_two_figs(main_window, clipper_ptr);
	// test_sablier(main_window, clipper_ptr);
	// test_idiot(main_window, clipper_ptr);
	// test_tr(main_window, clipper_ptr);

	/* Unlock and update the surface. */
	hw_surface_unlock(main_window);
	hw_surface_update_rects(main_window, NULL);

	/* Wait for a character on command line. */
	getchar();

	hw_quit();
	return (EXIT_SUCCESS);
}
