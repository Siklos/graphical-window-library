#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "hw_interface.h"
#include "ei_utils.h"
#include "ei_draw.h"
#include "ei_types.h"
#include "button_utils.h"
#include "widget_utils.h"


// void test_button(ei_surface_t surface)
// {
// 	ei_color_t color = { 0, 128, 255, 255 };
// 	int radius_angle = 23;
// 	ei_rect_t button_size = ei_rect(ei_point(200, 150), ei_size(400, 300));
// 	ei_linked_point_t *button = rounded_frame(button_size, radius_angle);
// 	ei_draw_polygon(surface, button, color, NULL);
// 	free_linked_point(button);
// }
//
// void test_button_up(ei_surface_t surface)
// {
// 	ei_color_t color_up = { 0, 128, 255, 255 };
// 	ei_color_t color_down = {128,255,0,255};
// 	int radius_angle = 30;
// 	ei_rect_t button_size_1 = ei_rect(ei_point(200, 150), ei_size(400, 300));
// 	ei_linked_point_t *button_u = button_up(button_size_1, radius_angle);
// 	ei_linked_point_t *button_d = button_down(button_size_1, radius_angle);
// 	ei_draw_polygon(surface, button_u, color_up, NULL);
// 	ei_draw_polygon(surface, button_d, color_down, NULL);
// 	free_linked_point(button_u);
// 	free_linked_point(button_d);
// }

// void drawing_button_plus(ei_surface_t surface)
// {
// 	ei_color_t color = {128,255,0,255};
// 	ei_color_t color_down = color_plus(color, 0.75);
// 	ei_color_t color_up = color_plus(color, 1.25);
// 	ei_color_t color_text = {255,255,255,255};
// 	int radius_angle = 30;
// 	int border_width = 20;
// 	ei_rect_t button_size = ei_rect(ei_point(200, 150), ei_size(400, 300));
// 	ei_point_t where = ei_point(250, 240);
//
// 	ei_linked_point_t in;
// 	ei_linked_point_t down;
// 	ei_linked_point_t up;
//
// 	// draw_button(surface, color_down, color_up, color, &in, &down, &up,
// 	// 		button_size, radius_angle, border_width, &where,
// 	// 		"BOUTON", ei_default_font, &color_text, NULL);
//
// 	draw_button(surface, color, &in, &down, &up,
// 			button_size, radius_angle, border_width, NULL);
// }

void drawing_button_normal(ei_surface_t surface)
{
	ei_color_t color = {82,127,186,255};
	ei_color_t color_down = lighten_color(color);
	ei_color_t color_up = darken_color(color);
	ei_color_t color_text = {255,255,255,255};
	int radius_angle = 30;
	int border_width = 20;
	ei_rect_t button_size = ei_rect(ei_point(200, 150), ei_size(400, 300));
	ei_point_t where = ei_point(300, 280);

	ei_linked_point_t in;
	ei_linked_point_t down;
	ei_linked_point_t up;

	draw_button(surface, color, color_down, color_up, &in, &down, &up,
			button_size, radius_angle, border_width, NULL);

	ei_draw_text(surface, &where, "UN BEAU BOUTON", ei_default_font, &color_text, NULL);
}

int ei_main(int argc, char **argv)
{
	ei_size_t win_size = ei_size(800, 600);
	ei_surface_t main_window = NULL;
	ei_color_t white = { 0xff, 0xff, 0xff, 0xff };
	ei_rect_t *clipper_ptr = NULL;
//      ei_rect_t               clipper         = ei_rect(ei_point(200, 150), ei_size(400, 300));
//      clipper_ptr             = &clipper;

	hw_init();

	main_window = hw_create_window(&win_size, EI_FALSE);

	/* Lock the drawing surface, paint it white. */
	hw_surface_lock(main_window);
	ei_fill(main_window, &white, clipper_ptr);

	/* Draw button */
	drawing_button_normal(main_window);

	/* Unlock and update the surface. */
	hw_surface_unlock(main_window);
	hw_surface_update_rects(main_window, NULL);

	/* Wait for a character on command line. */
	getchar();

	hw_quit();
	return (EXIT_SUCCESS);
}
