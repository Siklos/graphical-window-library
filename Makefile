# Variable definitions.

PLATFORM	= $(shell uname)
CC		= gcc
SRC   =   ./src/
OBJDIR		= ./objs
TESTS		= ./tests
INCLUDES	= ./include
INCFLAGS	:= -I${INCLUDES} -I${TESTS}
#OPTFLAGS	:= -Os -DNDEBUG
OPTFLAGS	:= -g
CCFLAGS		:= -c ${OPTFLAGS} -Wall -std=c99

# The list of objects to include in the library

LIBEIOBJS	:= ${OBJDIR}/ei_draw.o \
							${OBJDIR}/ei_event.o\
							${OBJDIR}/ei_toplevel.o \
							${OBJDIR}/ei_application.o \
							${OBJDIR}/ei_frame.o \
							${OBJDIR}/ei_widget.o \
							${OBJDIR}/ei_widgetclass.o \
							${OBJDIR}/ei_placer.o \
							${OBJDIR}/widget_utils.o \
							${OBJDIR}/button_utils.o \
							${OBJDIR}/bresenham_utils.o \
							${OBJDIR}/polygon_utils.o \
							${OBJDIR}/draw_utils.o \
							${OBJDIR}/ei_button.o \
							${OBJDIR}/ei_widget_extended.o

# Platform specific definitions (OS X, Linux)

ifeq (${PLATFORM},Darwin)

	# Building for Mac OS X

	PLATDIR		= _osx
	INCFLAGS	:= ${INCFLAGS} -I/opt/local/include/SDL
	LINK		= ${CC}
	LIBEI		= ${OBJDIR}/libei.a
	LIBEIBASE	= ${PLATDIR}/libeibase.a
	LIBS		= ${LIBEIBASE} -L/opt/local/lib -lSDL -lSDL_ttf -lSDL_image -framework AppKit
	CCFLAGS		:= ${CCFLAGS} -D__OSX__

else

	# Building for Linux

	PLATDIR		= _x11
	INCFLAGS	:= ${INCFLAGS} -I/usr/include/SDL
	LINK		= ${CC}
#	ARCH	        = 32
	ARCH	        = 64
	LIBEI		= ${OBJDIR}/libei.a
	LIBEIBASE	= ${PLATDIR}/libeibase${ARCH}.a
	LIBS		= ${LIBEIBASE} -lSDL -lSDL_ttf -lSDL_image -lm
	CCFLAGS		:= ${CCFLAGS} -D__LINUX__ -m${ARCH}
	LDFLAGS		= -m${ARCH} -g

endif



# Main target of the makefile. To build specific targets, call "make <target_name>"

TARGETS		=	${LIBEI} \
			minimal lines frame lot_frame button hello_world puzzle two048 text polygon draw_button toplevel double_toplevel
all : directories ${TARGETS}


#directories
.PHONY: directories
directories: ${OBJDIR}

${OBJDIR}:
	mkdir ${OBJDIR}

########## Test-programs

# minimal

minimal : ${OBJDIR}/minimal.o ${LIBEIBASE}
	${LINK} -o minimal ${LDFLAGS} ${OBJDIR}/minimal.o ${LIBS}

${OBJDIR}/minimal.o : ${TESTS}/minimal.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${TESTS}/minimal.c -o ${OBJDIR}/minimal.o

# lines

lines : ${OBJDIR}/lines.o ${LIBEIBASE} ${LIBEI}
	${LINK} -o lines ${OBJDIR}/lines.o ${LIBEI} ${LIBS}

${OBJDIR}/lines.o : ${TESTS}/lines.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${TESTS}/lines.c -o ${OBJDIR}/lines.o

# polygon

polygon : ${OBJDIR}/polygon.o ${LIBEIBASE} ${LIBEI}
	${LINK} -o polygon ${OBJDIR}/polygon.o ${LIBEI} ${LIBS}

${OBJDIR}/polygon.o : ${TESTS}/polygon.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${TESTS}/polygon.c -o ${OBJDIR}/polygon.o

# frame

frame : ${OBJDIR}/frame.o ${LIBEIBASE} ${LIBEI}
	${LINK} -o frame ${LDFLAGS} ${OBJDIR}/frame.o ${LIBEI} ${LIBS}

${OBJDIR}/frame.o : ${TESTS}/frame.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${TESTS}/frame.c -o ${OBJDIR}/frame.o

toplevel : ${OBJDIR}/toplevel.o ${LIBEIBASE} ${LIBEI}
	${LINK} -o toplevel ${LDFLAGS} ${OBJDIR}/toplevel.o ${LIBEI} ${LIBS}

${OBJDIR}/toplevel.o : ${TESTS}/toplevel.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${TESTS}/toplevel.c -o ${OBJDIR}/toplevel.o

# frame

lot_frame : ${OBJDIR}/lot_frame.o ${LIBEIBASE} ${LIBEI}
	${LINK} -o lot_frame ${LDFLAGS} ${OBJDIR}/lot_frame.o ${LIBEI} ${LIBS}

${OBJDIR}/lot_frame.o : ${TESTS}/lot_frame.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${TESTS}/lot_frame.c -o ${OBJDIR}/lot_frame.o



# button

button : ${OBJDIR}/button.o ${LIBEIBASE} ${LIBEI}
	${LINK} -o button ${LDFLAGS} ${OBJDIR}/button.o ${LIBEI} ${LIBS}

${OBJDIR}/button.o : ${TESTS}/button.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${TESTS}/button.c -o ${OBJDIR}/button.o

# hello_world

hello_world : ${OBJDIR}/hello_world.o ${LIBEIBASE} ${LIBEI}
	${LINK} -o hello_world ${LDFLAGS} ${OBJDIR}/hello_world.o ${LIBEI} ${LIBS}

${OBJDIR}/hello_world.o : ${TESTS}/hello_world.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${TESTS}/hello_world.c -o ${OBJDIR}/hello_world.o

# puzzle

puzzle : ${OBJDIR}/puzzle.o ${LIBEIBASE} ${LIBEI}
	${LINK} -o puzzle ${LDFLAGS} ${OBJDIR}/puzzle.o ${LIBEI} ${LIBS}

${OBJDIR}/puzzle.o : ${TESTS}/puzzle.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${TESTS}/puzzle.c -o ${OBJDIR}/puzzle.o


# two048

two048 : ${OBJDIR}/two048.o ${LIBEIBASE} ${LIBEI}
	${LINK} -o two048 ${LDFLAGS} ${OBJDIR}/two048.o ${LIBEI} ${LIBS}

${OBJDIR}/two048.o : ${TESTS}/two048.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${TESTS}/two048.c -o ${OBJDIR}/two048.o


# text

text : ${OBJDIR}/text.o ${LIBEIBASE} ${LIBEI}
	${LINK} -o text ${OBJDIR}/text.o ${LIBEI} ${LIBS}

${OBJDIR}/text.o : ${TESTS}/text.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${TESTS}/text.c -o ${OBJDIR}/text.o

# double_toplevel
double_toplevel : ${OBJDIR}/double_toplevel.o ${LIBEIBASE} ${LIBEI}
	${LINK} -o double_toplevel ${OBJDIR}/double_toplevel.o ${LIBEI} ${LIBS}

${OBJDIR}/double_toplevel.o : ${TESTS}/double_toplevel.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${TESTS}/double_toplevel.c -o ${OBJDIR}/double_toplevel.o

# draw_button
draw_button : ${OBJDIR}/draw_button.o ${LIBEIBASE} ${LIBEI}
	${LINK} -o draw_button ${OBJDIR}/draw_button.o ${LIBEI} ${LIBS}

${OBJDIR}/draw_button.o : ${TESTS}/draw_button.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${TESTS}/draw_button.c -o ${OBJDIR}/draw_button.o

# Building of the library libei

${LIBEI} : ${LIBEIOBJS}
	ar rcs ${LIBEI} ${LIBEIOBJS}

${OBJDIR}/ei_application.o: ${SRC}/ei_application.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${SRC}/ei_application.c -o ${OBJDIR}/ei_application.o

${OBJDIR}/ei_draw.o: ${SRC}/ei_draw.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${SRC}/ei_draw.c -o ${OBJDIR}/ei_draw.o

${OBJDIR}/ei_event.o: ${SRC}/ei_event.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${SRC}/ei_event.c -o ${OBJDIR}/ei_event.o

${OBJDIR}/ei_frame.o: ${SRC}/ei_frame.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${SRC}/ei_frame.c -o ${OBJDIR}/ei_frame.o

${OBJDIR}/ei_toplevel.o: ${SRC}/ei_toplevel.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${SRC}/ei_toplevel.c -o ${OBJDIR}/ei_toplevel.o

${OBJDIR}/ei_widget.o: ${SRC}/ei_widget.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${SRC}/ei_widget.c -o ${OBJDIR}/ei_widget.o

${OBJDIR}/ei_widget_extended.o: ${SRC}/ei_widget_extended.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${SRC}/ei_widget_extended.c -o ${OBJDIR}/ei_widget_extended.o

${OBJDIR}/ei_widgetclass.o: ${SRC}/ei_widgetclass.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${SRC}/ei_widgetclass.c -o ${OBJDIR}/ei_widgetclass.o

${OBJDIR}/button_utils.o: ${SRC}/button_utils.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${SRC}/button_utils.c -o ${OBJDIR}/button_utils.o

${OBJDIR}/bresenham_utils.o: ${SRC}/bresenham_utils.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${SRC}/bresenham_utils.c -o ${OBJDIR}/bresenham_utils.o

${OBJDIR}/polygon_utils.o: ${SRC}/polygon_utils.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${SRC}/polygon_utils.c -o ${OBJDIR}/polygon_utils.o

${OBJDIR}/widget_utils.o: ${SRC}/widget_utils.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${SRC}/widget_utils.c -o ${OBJDIR}/widget_utils.o

${OBJDIR}/draw_utils.o: ${SRC}/draw_utils.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${SRC}/draw_utils.c -o ${OBJDIR}/draw_utils.o

${OBJDIR}/ei_placer.o: ${SRC}/ei_placer.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${SRC}/ei_placer.c -o ${OBJDIR}/ei_placer.o

${OBJDIR}/ei_button.o: ${SRC}/ei_button.c
	${CC} ${CCFLAGS} ${INCFLAGS} ${SRC}/ei_button.c -o ${OBJDIR}/ei_button.o

# Building of the doxygen documentation.

doc :
	doxygen docs/doxygen.cfg



# Removing all built files.

clean:
	rm -f ${TARGETS}
	rm -f *.exe
	rm -f ${OBJDIR}/*
